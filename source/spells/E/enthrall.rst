.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--e--Enthrall:

Захватывающая речь (`Enthrall <http://2e.aonprd.com/Spells.aspx?ID=104>`_) / Закл. 3
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- очарование
- слуховое
- эмоции

**Обычай**: арканный, оккультный

**Использование**: |д-2| жестовый, словесный

**Дистанция**: 120 футов

**Цели**: все существа в радиусе

**Спасбросок**: Воля

**Продолжительность**: поддерживаемое

----------

Ваши слова завораживают ваши цели.
Вы говорите или поете без перерыва на протяжении колдовства и его продолжительности.
Цели, которые замечают вашу речь или песню, могут уделить свое безраздельное внимание; каждая цель должна пройти спасбросок Воли.
Мастер может предоставить бонус обстоятельства (максимум +4), если цель имеет противоположную религию, родословную или политические взгляды, или в противном случае вряд ли согласится с тем, что вы говорите.

Каждое существо попавшее в радиус действия, должно попытаться пройти спасбросок Воли когда вы используете :ref:`action--Sustain-a-Spell`.
Если вы говорите, *захватывающая речь* получает языковой признак.

| **Критический успех**: Цель невредима и она замечает, что вы пытались использовать магию.
| **Успех**: Цель не должна обращать внимания, но не замечает, что вы пытались использовать магию (она может заметить, что другие поддались эффекту).
| **Провал**: Цель заворожена вами. Она может попытаться пройти другой спасбросок Воли, если станет свидетелем действий или речи, с которыми она не согласна. Если ей удается, она перестает быть завороженной и временно иммунна к эффекту на 1 час. Если цель подвергается враждебному действию или если другому существу удается пройти проверку Дипломатии или Запугивания против нее, заворожение немедленно прекращается.
| **Критический провал**: Как провал, но если цель с вами не согласна, она не может пройти повторной спасбросок, чтобы закончить заворожение.





.. include:: /helpers/actions.rst