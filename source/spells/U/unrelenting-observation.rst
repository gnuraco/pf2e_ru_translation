.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--u--Unrelenting-Observation:

Неутомимое наблюдение (`Unrelenting Observation <https://2e.aonprd.com/Spells.aspx?ID=351>`_) / Закл. 8
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- прорицание
- видение

**Обычай**: арканный, оккультный

**Использование**: |д-2| жестовый, словесный

**Дистанция**: 100 футов

**Область**: 20-футовый взрыв

**Цели**: 1 отслеживаемое существо или объект, и вплоть до 5 других готовых существ

**Продолжительность**: различается

----------

Это заклинание дает идеальный обзор, основанное на гадании, позволяя нескольким готовым целям отслеживать точные движения или положение одного существа или объекта.
Выберите целью для отслеживания одно существо или объект в области.
Это становится **сенсором** для заклинания.
Вплоть до 5 готовых существ по вашему выбору, могут видеть призрачный образ этого существа или объекта, когда он вне их видимости.
Они могут идеально воспринимать существо или объект, что позволяет им игнорировать состояние "скрыт" или "невидимость", хотя физические преграды все еще дают укрытия.

Следящие существа могут видеть отслеживаемое существо или объект через все преграды, за исключением свинца и текущей воды, которые блокируют их.
Расстояние не важно, однако существо или объект могут отдалиться так сильно, что станут слишком маленькими чтобы их заметить.
Следящие существа не видят никакое окружение вокруг цели, хотя они видят снаряжение, которое носит или держит цель, и они могут сказать если она сняла с себя что-то.

Если отслеживаемая цель является готовой, то продолжительность заклинания 1 час.
Если вы пытаетесь отслеживать неготовое существо, цель должна сделать спасбросок Воли.

| **Критический успех**: Существо или объект невредимы.
| **Успех**: Как описано, и продолжительность заклинания 1 минута.
| **Провал**: Как описано, и продолжительность заклинания 1 час.





.. include:: /helpers/actions.rst