.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--r--Regenerate:

Регенерация (`Regenerate <https://2e.aonprd.com/Spells.aspx?ID=248>`_) / Закл. 7
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- некромантия
- исцеление
- позитивное

**Обычай**: сакральный, природный

**Использование**: |д-2| жестовый, словесный

**Дистанция**: касание

**Цели**: 1 готовое живое существо

**Продолжительность**: 1 минута

----------

Вливание позитивной энергии дает существу непрерывное исцеление.
Цель временно получает регенерацию 15, которая восстанавливает ей 15 ОЗ в начале каждого ее хода.
Пока действует регенерация, цель не может умереть от урона и ее значение состояния "при смерти" не может превысить 3, однако если ее значение "при смерти" становится 4 или выше, она остается без сознания ее раны не будут исцелены.
Если цель получает урон кислотой или огнем, ее регенерация отключается до конца ее следующего хода.

Каждый раз, существо восстанавливает ОЗ от регенерации, оно так же выращивает один поврежденный или разрушенный орган (если такой есть).
Во время действия заклинания, существо может так же присоединить отсеченные части тела, потратив действие на :ref:`action--Interact` чтобы удержать часть тела в области откуда она была отсечена.

----------

**Усиление (9-й)**: Регенерация увеличивается до 20.





.. include:: /helpers/actions.rst