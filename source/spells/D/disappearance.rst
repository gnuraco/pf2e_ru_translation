.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--d--Disappearance:

Исчезновение (`Disappearance <https://2e.aonprd.com/Spells.aspx?ID=73>`_) / Закл. 8
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- иллюзия

**Обычай**: арканный, оккультный

**Использование**: |д-2| жестовый, материальный

**Дистанция**: касание

**Цели**: 1 существо

**Продолжительность**: 10 минут

----------

Вы скрываете существо от чужих чувств.
Цель становится "необнаруженной", не только для зрения, но и для всех других чувств, что позволяет цели считаться невидимой независимо от того какие точные и неточные чувства могут быть у наблюдателя.
Существу все еще возможно найти цель используя :ref:`action--Seek`, высматривая следы на пыли, вслушиваясь в недостающие части звукового окружения или находя какой-то другой способ обнаружить присутствие, необнаруженного существа.





.. include:: /helpers/actions.rst