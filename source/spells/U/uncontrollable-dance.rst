.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--u--Uncontrollable-Dance:

Неудержимый танец (`Uncontrollable Dance <https://2e.aonprd.com/Spells.aspx?ID=347>`_) / Закл. 8
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- очарование
- недееспособность
- ментальное

**Обычай**: арканный, оккультный

**Использование**: |д-2| жестовый, словесный

**Дистанция**: касание

**Цели**: 1 существо

**Спасбросок**: Воля

**Продолжительность**: различается

----------

Цель борется с всепоглощающим желанием танцевать.
На время действия заклинания, цель застигнута врасплох и не может использовать реакции.
Пока существо подвергается воздействию, оно не может использовать действия с признаком "перемещение" кроме как танцевать, используя действие :ref:`action--Stride` чтобы двигаться с половиной Скорости.

| **Критический успех**: Цель невредима.
| **Успех**: Продолжительность заклинания 3 раунда, и цель должна потратить минимум 1 действие каждый ход чтобы танцевать.
| **Провал**: Продолжительность заклинания 1 минута, и цель должна потратить минимум 2 действие каждый ход чтобы танцевать.
| **Критический провал**: Продолжительность заклинания 1 минута, и цель должна потратить все действия каждый ход чтобы танцевать.





.. include:: /helpers/actions.rst