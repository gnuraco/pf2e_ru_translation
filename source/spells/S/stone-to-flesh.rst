.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--s--Stone-to-Flesh:

Превращение камня в плоть (`Stone to Flesh <https://2e.aonprd.com/Spells.aspx?ID=311>`_) / Закл. 6
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- превращение
- земля

**Обычай**: сакральный, природный

**Использование**: |д-2| жестовый, словесный

**Дистанция**: касание

**Цели**: окаменевшие существа или каменные объекты человеческого размера

----------

Манипулируя фундаментальными частицами материи, вы превращаете камень в плоть и кровь.
Вы возвращаете окаменевшее существо в его нормальное состояние или превращаете каменный объект в массу инертной плоти (без Твердости камня) примерно такой же формы.





.. include:: /helpers/actions.rst