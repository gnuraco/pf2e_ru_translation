.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--c--Crushing-Despair:

Сокрушительное отчаяние (`Crushing Despair <https://2e.aonprd.com/Spells.aspx?ID=57>`_) / Закл. 5
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- очарование
- эмоция
- ментальное

**Обычай**: арканный, оккультный

**Использование**: |д-2| жестовый, словесный

**Область**: 30-футовый конус

**Спасбросок**: Воля

**Продолжительность**: 1 или более раундов

----------

Вы вызываете отчаяние у существ в области.
Эффекты для каждого существа определяются его спасброском Воли.

| **Критический успех**: Существо невредимо.
| **Успех**: На 1 раунд существо не может использовать реакции и должно пройти еще один спасбросок в начале его хода; при провале, оно "замедлено 1" на этот ход, так как оно безудержно рыдает.
| **Провал**: Как успех, но "замедлен 1" длится 1 минуту.
| **Критический провал**: Как провал, и существо автоматически "замедлено 1" на 1 минуту.

----------

**Усиление (7-й)**: Область увеличивается до 60-футового конуса.





.. include:: /helpers/actions.rst