.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--v--Vampiric-Exsanguination:

Вампирское обескровливание (`Vampiric Exsanguination <https://2e.aonprd.com/Spells.aspx?ID=353>`_) / Закл. 6
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- некромантия
- смерть
- негативное

**Обычай**: арканный, сакральный, оккультный

**Использование**: |д-2| жестовый, словесный

**Область**: 30-футовый конус

**Спасбросок**: простая Стойкость

----------

Своими вытянутыми руками, вы втягиваете кровь и жизненную силу других существ.
Вы наносите 12d6 негативного урона живым существам в области.

Вы получаете временные ОЗ, равные половине урона одного существа; считайте эти временные ОЗ используя существо, которое получило наибольший урон.
Вы теряете оставшиеся временные ОЗ через 1 минуту.

----------

**Усиление (+1)**: Урон увеличивается на 2d6.





.. include:: /helpers/actions.rst