.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--r--Repulsion:

Отторжение (`Repulsion <https://2e.aonprd.com/Spells.aspx?ID=254>`_) / Закл. 6
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- преграждение
- аура
- ментальное

**Обычай**: арканный, сакральный, оккультный

**Использование**: |д-2| жестовый, словесный

**Дистанция**: эманация вплоть до 40 футов

**Спасбросок**: Воля

**Продолжительность**: 1 минута

----------

Вы испускаете ауру, которая не дает существам приблизиться к вам.
Когда колдуете заклинание, вы можете сделать область любого радиуса, на свой выбор, вплоть до 40 футов.
Существо должно пройти спасбросок Воли, если оно в области когда вы колдовали, или как только оно входит в область, пока заклинание действует.
Как только существо прошло спасбросок, оно использует тот же результат для того же эффекта *отторжения*.
Любые ограничения на движение существа действуют только в том случае, если оно добровольно движется к вам.
Например, если вы двигаетесь к существу, оно не обязано отдаляться.

| **Критический успех**: Движения существа не ограничены.
| **Успех**: Существо, приближаясь к вам, считает каждый квадрат в области как сложную местность.
| **Провал**: Существо не может подходить к вам в пределах области.





.. include:: /helpers/actions.rst