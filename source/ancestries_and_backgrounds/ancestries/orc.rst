.. include:: /helpers/roles.rst

.. rst-class:: ancestry
.. _ancestry--Orc:

Орк (Orc)
=============================================================================================================

.. epigraph::
	
	*Орки закаляются в огне насилия и конфликтов, часто с момента своего рождения.
	Живя жизнью, которая часто жестоко обрывается, орки упиваются тем, что испытывают свои силы против достойных врагов, бросая вызов высокопоставленному члену своего сообщества за господство или совершая набег на соседнее поселение.
	Многие орки ищут славы, как только они начинают ходить и могут носить меч или дубину, укрощая диких зверей или охотясь на смертельных монстров.*

-----------------------------------------------------------------------------

.. rst-class:: sidebar-char-ancestry-class

.. sidebar:: hidden

	.. rubric:: Редкость

	Необычный


	.. rubric:: Очки здоровья

	10


	.. rubric:: Размер

	Средний


	.. rubric:: Скорость

	25 футов


	.. rubric:: Повышения характеристик

	Сила

	Свободное


	.. rubric:: Языки

	Всеобщий

	Орочий

	Дополнительные языки в количестве вашего модификатора Интеллекта (если положительный).
	Выберите из Гоблинский, Йотун, Земляной и Подземный, и тех к которым у вас есть доступ (распространенные в вашем регионе).


	.. rubric:: Признаки

	Орк

	Гуманоид


	.. rubric:: Ночное зрение

	Вы можете видеть в темноте и при тусклом свете, как если бы это был яркий свет, однако ваше зрение в темноте - черно-белое.



Орки часто борются за признание среди других сообществ, которые часто видят в них животных.
Однако те, кто заслужил верность друга-орка, вскоре узнают, что его верность и честность не имеют себе равных.
Орки варвары, воины и рейнджеры ценятся как гладиаторы и наемники.
В то время как некоторые человеческие поселения могут не решаться принять в свои ряды целое сообщество орков, небольшая горстка наемников орков может выполнять работу целого отряда людей-новобранцев, пока орки хорошо кормятся и хорошо платят.
Хотя вера в то, что орки подходят только для битвы, широко распространена среди других гуманоидов, суровый менталитет орков проистекает из долгой истории конфликтов, а не из-за отсутствия способностей в других областях.

Культура орков учит, что они сформированы испытаниями, которые они переживают, и самые достойные переживают большинство невзгод.
Орки, достигшие как долгой жизни, так и великих триумфов, вызывают огромное уважение.

Если вам нужен персонаж, который будет выносливым, грозным и превосходным в физических задачах, вам следует сыграть орком.



.. rst-class:: h3
.. rubric:: Вы можете ...

* С нетерпением встретить любой шанс доказать свою силу в физическом соревновании
* Верить, что ложь и предательство - для тех, у кого не хватает сил взять то, что они хотят
* Рассматривать смерть в славном бою как более предпочтительную, чем обычную смерть от старости или болезни


.. rst-class:: h3
.. rubric:: Другие возможно ...

* Считают вас жестоким или недисциплинированным
* Недооценивают ваш интеллект, хитрость и знания
* Восхищаются вашей прямотой и откровенной честностью



.. rst-class:: h3
.. rubric:: Физическое описание

Орки высокие и крепко сложенные, с длинными руками и коренастыми ногами.
Многие орки достигают 7 футов в высоту, хотя они склонны принимать широкие, почти кривоногие позы и сутулиться вперед в плечах.
Эта комбинация создает кажущееся противоречие, когда уровень глаз находится там же где и у большинства гуманоидов, в то же время возвышаясь над ними.
У орков грубая кожа, толстые кости и твердые как камень мышцы, что делает их пригодными для войны и других физически сложных задач.
Несмотря на грубую кожу, орки легко получают шрамы, и большинство орков очень гордятся своими шрамами.
Цвет кожи орков обычно зеленый, а иногда и серый, хотя у некоторых орков есть другие цвета кожи, которые отражают адаптацию к их среде обитания.

Орки считают сильное телосложение и покрытую шрамами кожу привлекательной, независимо от пола.
Могущественный орк имеет более сильное влияние, а шрамы это знаки одержанных побед или пережитых трудностей.
Точно так же многие орки считают большие выступающие клыки более привлекательными, чем маленькие, большие являются более эффективным оружием.
Многие орки также находят татуировки привлекательными, особенно большие или болезненные, которые покрывают значительное количество кожи.



.. rst-class:: h3
.. rubric:: Общество

Большинство сообществ орков определяют себя через две вещи: боль и славу.
Каждый заслуживает уважения почти в равной мере, пока боль переносится со стоицизмом.
Орк со множеством шрамов, который безропотно ходит со сломанной ногой, вызывает такое же восхищение, как и тот, кто одерживает великую победу на поле боя.

Власть в орочьем владении происходит от силы или семейного рода.
Структура, как правило, феодальная, с более слабыми орками, работающими по указке сильных.
Крепость Белькзен (The Hold of Belkzen) - самое большое такое общество, и власть там быстро переходит из рук в руки.
Один могучий орк, умирающий в битве, может встряхнуть всю структуру власти, что приведет к ссорам и дуэлям за контроль над ней.
Многие орки, которые устают быть подчиненными, отделяются, чтобы сформировать свои собственные отряды, отправляясь на новые территории.

Молодые орки обычно воспитываются всей общиной.
В самом деле, для орков было бы почти невозможно вырастить своих детей каким-либо другим способом, поскольку близнецы, тройняшки и даже четверни довольно распространены в семьях орков, как и смерть среди орков в их детородные годы.
Многие орк-холды проводят церемонии, когда молодой орк достигает совершеннолетия, обычно около их десятого или одиннадцатого дня рождения, во время которых им, новым взрослым рассказывают, какова будет их роль в крепости.
Для общин, практикующих ритуальное шрамирование или татуировку, это часто происходит, когда молодые орки получают свой первый шрам-крепости или татуировку.

Орки мало чего боятся, но большинство не доверяют магии.
Магия рассматривается как инструмент, который обходит физическое и позволяет слабым бороться с сильными, и это убеждение, которая противоречит ценностям орков.
Хотя они уважают физическую мощь :ref:`Горумских <Deity--Gorum>` боевых жрецов и даже друидов, которые принимают облик великих зверей, они считают арканную и оккультную магию сомнительной в лучшем случае и неэтичной в целом.
Все сообщества орков, кроме самых порочных, рассматривают некромантию как скверное искусство, крадущее славу у мертвых, и их растущая борьба с нежитью дала им новые точки соприкосновения с их гуманоидными соседями.



.. rst-class:: h3
.. rubric:: Мировоззрение и религия

Распространенная орочья поговорка: "Ты - шрамы, которые формируют тебя".
Жестокие, хаотичные жизни в жестоких, хаотичных землях означают, что большинство орков тяготеют к хаотичному мировоззрению.
:ref:`Горум <Deity--Gorum>`, :ref:`Ламашку <Deity--Lamashtu>` и :ref:`Ровагуг <Deity--Rovagug>` наиболее почитаемые в орочьих сообществах, хотя менее жестокие придерживаются поклонения природным божествам, таким как :ref:`Гозрей <Deity--Gozreh>`, или богам, таким как :ref:`Саренрэй <Deity--Sarenrae>`, чьи догматы огня, искупления и славы имеют некоторую привлекательность для чувств орков.

Хотя существуют орочьи божества, поклонение им удивительно необычно среди орков.
Орки верят, что если у существа есть лицо и имя, его можно убить, и поэтому их собственные божества являются целями, а не объектами почитания.
Некоторые орочьи общества учат, что их величайшие представители могут заработать шанс бросить вызов орочьим божествам за место в пантеоне.
Большинство орков не тратят впустую свои предсмертные мгновения, восхваляя божество или молясь о месте в загробной жизни, но сквозь сломанные зубы, выплевывают кровавое предупреждение своим божествам, обещая им нового соперника.



.. rst-class:: h3
.. rubric:: Орки-авантюристы (Orc Adventurers)

Стремление орка преодолеть трудности и проявить себя побуждает многих стать искателями приключений, хотя орки с большей вероятностью отправятся в путь сами по себе или вместе с другими орками, чем вместе с искателями приключений других родословных.

Распространенные предыстории орков это гладиатор, охотник, ученик боевых искусств, кочевник, воин, бандит, дозорный и беженец.
Орки преуспевают в боевых классах, таких как варвар и воин.



.. rst-class:: h3
.. rubric:: Имена

У орков резкий гортанный язык, и их правила именования не являются исключением.
Многие имена орков - это просто орочьи слова, обозначающие особенно желательные черты, такие как огромная сила, рост или свирепость.
Обычно в качестве своей фамилии орки используют либо название своей крепости, либо имя, отсылающее к особо запоминающемуся достижению.

.. rst-class:: h4
.. rubric:: Пример имен

Аркус, Аск, Дарра, Граск, Гриллгисс, Крагга, Маджа, Мурдут, Оллак, Онята, Турк, Уирч, Унач






Наследия орков (Orc Heritages)
-----------------------------------------------------------------------------------------------------------

Орки имеют долгую историю на поверхности мира и гораздо более долгую историю под землей.
Их выносливое телосложение и требовательный образ жизни привели к различным адаптациям.
На 1-м уровне выберите одно из следующих орочьих наследий.

.. _ancestry-heritage--Orc--Badlands:

Орк пустошей (`Badlands Orc <https://2e.aonprd.com/Heritages.aspx?ID=68>`_)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Источник**: Advanced Player's Guide pg. 18

Вы родом из выжженных солнцем бесплодных земель, где вам помогли преуспеть длинные ноги и способность противостоять стихиям.
Вы можете :ref:`expl-activity--Hustle` в два раза дольше, пока исследуете, прежде чем вам надо остановиться, и вы считаете эффекты жаркой окружающей среды так, как если бы они были на одну ступень ниже (невероятная жара становится экстремальным, экстремальная жара становится суровой и так далее).


.. _ancestry-heritage--Orc--Deep:

Глубинный орк (`Deep Orc <https://2e.aonprd.com/Heritages.aspx?ID=69>`_)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Источник**: Advanced Player's Guide pg. 18

Ваши мозолистые руки и красные глаза говорят о жизни, проведенной в кромешной темноте горных пещер, где вы научились сражаться на скалистых утесах и выживать с минимальными ресурсами.
Вы получаете способности навыка :ref:`feat--Terrain-Expertise` для подземной местности и :ref:`feat--Combat-Climber`.


.. _ancestry-heritage--Orc--Hold-Scarred:

Орк со шрамами крепости (`Hold-Scarred Orc <https://2e.aonprd.com/Heritages.aspx?ID=70>`_)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Источник**: Advanced Player's Guide pg. 18

Вы являетесь частью сообщества орков, которое практикует ритуальное шрамирование или татуирование.
Отметины на вашей коже показывают вашу исключительную выносливость и жизненную силу.
Вы получаете 12 ОЗ от своей родословной вместо 10.
Вы так же получаете способность :ref:`feat--Diehard`.


.. _ancestry-heritage--Orc--Rainfall:

Орк тропического леса (`Rainfall Orc <https://2e.aonprd.com/Heritages.aspx?ID=71>`_)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Источник**: Advanced Player's Guide pg. 18

Вы родились в тропическом лесу, где только сплетение деревьев обеспечивает защиту от проливных ливней и внезапных наводнений.
Вы научились ловко передвигаться по джунглям и противостоять различным болезням, распространенным во влажных окрестностях.
Вы получаете бонус обстоятельства +2 к проверкам Атлетики, чтобы :ref:`skill--Athletics--Swim` или :ref:`skill--Athletics--Climb` и бонус обстоятельства +1 к спасброскам против болезней.



Универсальные наследия (Versatile Heritages)
-----------------------------------------------------------------------------------------------------------

.. include:: /helpers/versatile_heretages.rst










.. rst-class:: ancestry-class-feats
.. _ancestry--Orc-Feats:

Способности родословной
-----------------------------------------------------------------------------------------------------------

На 1-м уровне вы получаете одну способность родословной, и получаете дополнительную каждые 4 уровня после этого (на 5-м, 9-м, 13-м и 17-м уровнях).
Как орк, вы выбираете из следующих способностей, а так же способностей с признаком "орк" из :ref:`полуорочьего наследия <ancestry--Half-Orc-Feats>`.


1-й уровень
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. _ancestry-feat--Orc--Beast-Trainer:

Дрессировщик зверей (`Beast Trainer <https://2e.aonprd.com/Feats.aspx?ID=1283>`_) / 1
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- орк

**Источник**: Advanced Player's Guide pg. 18

----------

У вас впечатляющая врожденная способность приручать свирепых зверей и командовать ими.
Вы становитесь обучены навыку Природы и получаете способность навыка :ref:`feat--Train-Animal`.


.. _ancestry-feat--Orc--Iron-Fists:

Железные кулаки (`Iron Fists <https://2e.aonprd.com/Feats.aspx?ID=1284>`_) / 1
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- орк

**Источник**: Advanced Player's Guide pg. 18

----------

Ваши кулаки выкованы битвой, ваша от природы жесткая кожа и плотные кости еще больше укрепились в результате конфликта.
Ваши безоружные атаки кулаками больше не имеют признака "несмертельное" и получают признак оружия "толчок".


.. _ancestry-feat--Orc--Orc Lore:

Знания орков (`Orc Lore <https://2e.aonprd.com/Feats.aspx?ID=1285>`_) / 1
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- орк

**Источник**: Advanced Player's Guide pg. 18

----------

Старейшины крепости учили вас истории своего народа, рассказывали истории о великих физических подвигах и делились с вами трудностями, которые пережили ваши предки, чтобы вы могли передать эту мудрость будущим поколениям.
Вы становитесь обучены Атлетике и Выживанию.
Если вы автоматически станете обучены одному из этих навыков (например, из-за вашей предыстории или класса), то вместо этого становитесь обучены другому навыку на свой выбор.
Вы так же становитесь обучены Знаниям орков.


.. _ancestry-feat--Orc--Tusks:

Клыки (`Tusks <https://2e.aonprd.com/Feats.aspx?ID=1286>`_) / 1
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- орк

**Источник**: Advanced Player's Guide pg. 18

----------

У вас особенно длинные, зазубренные клыки, идеально подходящие для отрывания мяса от костей.
Вы получаете безоружную атаку клыками, которая наносит 1d6 колющего урона.
Ваши клыки относятся к группе "драка", и имеют признаки "быстрое" и "безоружная".

**Особенность**: Вы можете выбрать эту способность только на 1-м уровне и не можете переизучить другую способность на эту.
Вы можете переизучить эту способность на другую только с помощью радикальных мер, таких как отламывание или спиливание ваших клыков.





5-й уровень
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. _ancestry-feat--Orc--Athletic-Might:

Атлетическая мощь (`Athletic Might <https://2e.aonprd.com/Feats.aspx?ID=1287>`_) / 5
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- орк

**Источник**: Advanced Player's Guide pg. 18

----------

Выживание во враждебной местности дало вам большой талант к мобильности.
Всякий раз, когда при броске проверки Атлетики на :ref:`skill--Athletics--Swim` или :ref:`skill--Athletics--Climb` вы получаете успех, то вместо этого вы получаете крит.успех.


.. _ancestry-feat--Orc--Bloody-Blows:

Кровавые удары (`Bloody Blows <https://2e.aonprd.com/Feats.aspx?ID=1288>`_) / 5
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- орк

**Источник**: Advanced Player's Guide pg. 18

----------

Ваши смертельные безоружные атаки оставляют кровавые раны или вызывают сильное внутреннее кровотечение.
Когда вы критически попадаете :ref:`Ударом (Strike) <action--Strike>` используя безоружную атаку, которая не несмертельная, цель получает 1d4 продолжительного урона кровотечением.
Это может быть потому что вы получаете штраф для использования кулаков для смертельной атаки, или потому что у вас есть безоружная атака без признака "несмертельная" от :ref:`ancestry-feat--Orc--Iron-Fists`, :ref:`ancestry-feat--Orc--Tusks` или подобной способности.


.. _ancestry-feat--Orc--Hold-Mark:

Метка крепости (`Hold Mark <https://2e.aonprd.com/Feats.aspx?ID=1289>`_) / 5
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- орк

**Источник**: Advanced Player's Guide pg. 18

----------

У вас есть шрамы или татуировки, свидетельствующие о доблести вашего сообщества.
При выборе этой способности, выберите один из вариантов представленных далее.
Когда вы критически попадаете используя оружие выбранной группы, то вы применяете эффект критической специализации оружия.
Вы получаете большое клеймо или татуировку в виде выбранной эмблемы или аналогичного изображения (например, топор может быть медведем или другим символом свирепости, а щит может быть черепахой или другим символом, связанным с сильной защитой) и получаете перечисленные преимущества.

* **Топор**: топор или клевец
* **Щит**: молот или щит
* **Факел**: бомба или нож





9-й уровень
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. _ancestry-feat--Orc--Deaths-Drums:

Барабаны смерти (`Death's Drums <https://2e.aonprd.com/Feats.aspx?ID=1290>`_) / 9
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- орк

**Источник**: Advanced Player's Guide pg. 19

----------

Ваша жизнь была потрачена на то, чтобы бросить вызов самой смерти, а близость к этому непримиримому противнику только заставляет ваше сердце биться сильнее.
Когда вы получаете продолжительный урон или значение вашего состояние "ранен" равно 1 или больше, вы получаете бонус обстоятельства +2 к спасброскам Стойкости.


.. _ancestry-feat--Orc--Undying-Ferocity:

Неумирающая свирепость (`Undying Ferocity <https://2e.aonprd.com/Feats.aspx?ID=1291>`_) / 9
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- орк

**Предварительные условия**: :ref:`ancestry-feat--HalfOrc--Orc-Ferocity`

**Источник**: Advanced Player's Guide pg. 19

----------

Вы сопротивляетесь лапам смерти со сверхъестественной силой.
Когда вы используете :ref:`ancestry-feat--HalfOrc--Orc-Ferocity`, то получаете временные ОЗ равные вашему уровню.





13-й уровень
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. _ancestry-feat--Orc--Ferocious-Beasts:

Свирепые чудовища (`Ferocious Beasts <https://2e.aonprd.com/Feats.aspx?ID=1292>`_) / 13
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- орк

**Предварительные условия**: :ref:`ancestry-feat--Orc--Beast-Trainer` или зверь-компаньон, :ref:`ancestry-feat--HalfOrc--Orc-Ferocity`

**Источник**: Advanced Player's Guide pg. 19

----------

С древних времен самые могучие укротители орков пробуждали истинный боевой дух своих зверей-компаньонов, поя их напитком, содержащим собственную кровь орка.
Звери-компаньоны или привязанные животные, которые у вас есть, получают способность :ref:`ancestry-feat--HalfOrc--Orc-Ferocity`, и получают реакцию, которую они могут использовать только для "Орочей свирепости".
Если у вас есть способность родословной :ref:`ancestry-feat--Orc--Undying-Ferocity`, то все звери-компаньоны или привязанные животные, которые у вас есть, так же получают преимущества этой способности, когда используют реакцию "Орочей свирепости".


.. _ancestry-feat--Orc--Spell-Devourer:

Пожиратель заклинаний (`Spell Devourer <https://2e.aonprd.com/Feats.aspx?ID=1293>`_) / 13
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- орк

**Предварительные условия**: :ref:`ancestry-feat--HalfOrc--Pervasive-Superstition`

**Источник**: Advanced Player's Guide pg. 19

----------

Вы не просто сопротивляетесь магии, вы пожираете ее.
Всякий раз, когда вы успешно совершаете спасбросок против заклинания или магического эффекта, вы получаете временные ОЗ, равные удвоенному уровню заклинания, или просто уровню, если магический эффект не заклинание.
Эти временные ОЗ длятся до конца вашего следующего хода.

**Заметка**: Временные ОЗ применяются как только персонаж успешно совершил спасбросок.
Для эффекта, который наносит урон ОЗ при успехе, как например *огненный шар*, это значит, что персонаж получает временные ОЗ до получения урона.





17-й уровень
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. _ancestry-feat--Orc--Rampaging-Ferocity:

Неистовая свирепость (`Rampaging Ferocity <https://2e.aonprd.com/Feats.aspx?ID=1294>`_) |д-св| / 17
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- орк

**Предварительные условия**: :ref:`ancestry-feat--HalfOrc--Orc-Ferocity`

**Триггер**: Вы используете :ref:`ancestry-feat--HalfOrc--Orc-Ferocity`

**Источник**: Advanced Player's Guide pg. 19

----------

Вы злобно набрасываетесь на врага, даже когда боретесь со смертью.
Сделайте один :ref:`action--Strike`.
Если этот Удар опустит ОЗ врага до 0, то эта активация :ref:`ancestry-feat--HalfOrc--Orc-Ferocity` не считается к ее количеству использований в день.





.. include:: /helpers/actions.rst