.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--w--Warp-Mind:

Искривление разума (`Warp Mind <https://2e.aonprd.com/Spells.aspx?ID=369>`_) / Закл. 7
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- очарование
- недееспособность
- эмоция
- ментальное

**Обычай**: арканный, оккультный

**Использование**: |д-2| жестовый, словесный

**Дистанция**: 120 футов

**Цели**: 1 существо

**Спасбросок**: Воля

----------

Вы перемешиваете умственные способности существа и сенсорную информацию.
Цель должна сделать спасбросок Воли.
Независимо от результата спасброска, цель временно иммунна на 10 минут.
Эффект *искривления разума* наступает мгновенно, так что :ref:`spell--d--Dispel-Magic` и другие эффекты, которые противодействуют заклинаниям, не могут противодействовать ему.
Однако, :ref:`spell--a--Alter-Reality`, :ref:`spell--m--Miracle`, :ref:`spell--p--Primal-Phenomenon`, :ref:`spell--w--Wish` или :ref:`spell--r--Restoration` все же могут противодействовать эффектам.

| **Критический успех**: Цель невредима.
| **Успех**: Цель тратит первое действие своего следующего хода, с состоянием "замешательство".
| **Провал**: Цель получает состояние "замешательство" на 1 минуту.
| **Критический провал**: Цель получает состояние "замешательство" навсегда.





.. include:: /helpers/actions.rst