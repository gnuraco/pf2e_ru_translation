.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--f--Fabricated-Truth:

Сфабрикованная правда (`Fabricated Truth <https://2e.aonprd.com/Spells.aspx?ID=106>`_) / Закл. 10
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- очарование
- недееспособность
- ментальное

**Обычай**: оккультный

**Использование**: |д-3| жестовый, словесный, материальный

**Дистанция**: 100 футов

**Цели**: вплоть до 5 существ

**Спасбросок**: Воля

**Продолжительность**: различается

----------

Выберите одно утверждение, в которое вы хотите, чтобы цели поверили.
Этот факт может быть конкретным, например "дракон кружит сверху и хочет убить меня"; обширным, например: "все гуманоиды - замаскированные отвратительные твари"; или концептуальным, например: "если я не проживу жизнь добрым, то буду наказан в загробной жизни".
Опыт целей влияет на то, как они реагируют на эту "правду" и как меняется их поведение.
Если утверждение изменяет то, что они воспринимают, они считают это изменение как внезапное откровение.

Эффект заклинания зависит от спасброска Воли цели.
Если цель уже подвержена воздействию *закл*, то ваше заклинание пытается противодействовать ему.
Если проверка противодействия проваливается, результат спасброска цели не может быть хуже чем успех.

| **Критический успех**: Цель не верит утверждению и знает что вы пытались одурачить ее.
| **Успех**: Цель не верит утверждению или осознает что вы пытались одурачить ее.
| **Провал**: Цель верит утверждению 1 неделю.
| **Критический провал**: Цель верит утверждению неограниченное время.





.. include:: /helpers/actions.rst