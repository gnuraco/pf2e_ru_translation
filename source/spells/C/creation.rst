.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--c--Creation:

Созидание (`Creation <http://2e.aonprd.com/Spells.aspx?ID=54>`_) / Закл. 4
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- воплощение

**Обычай**: арканный, природный

**Использование**: 1 минута (жестовый, словесный, материальный)

**Дистанция**: 0 футов

**Продолжительность**: 1 час

----------

Вы создаете временный объект из сверхъестественной энергии.
Он должен быть растительного происхождения (например, древесина или бумага), 5 кубических футов или меньше.
Он не может иметь вычурных художественных деталей или сложных подвижных части, не может иметь ценность, или быть изготовлен из драгоценных материалов или материалов необычной редкости и лучше.
Является очевидным, что предмет наколдован и является временным, и поэтому не может быть продан или выдан за подлинный.

----------

**Усиление (5-й)**: Предмет металлический и может включать в себя обычные минералы, такие как полевой шпат или кварц.





.. include:: /helpers/actions.rst