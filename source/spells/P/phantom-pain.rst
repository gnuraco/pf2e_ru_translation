.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--p--Phantom-Pain:

Фантомная боль (`Phantom Pain <http://2e.aonprd.com/Spells.aspx?ID=220>`_) / Закл. 1
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- иллюзия
- ментальное
- несмертельное

**Обычай**: оккультный

**Использование**: |д-2| жестовый, словесный

**Дистанция**: 30 футов

**Цели**: 1 существо

**Спасбросок**: Воля

**Продолжительность**: 1 минута

----------

Иллюзорная боль поражает цель, нанося 2d4 ментального урона и 1d4 продолжительного ментального урона.
Цель должна пройти спасбросок Воли.

| **Критический успех**: Цель невредима.
| **Успех**: Цель получает полный начальный урон, но не продолжительный, и заклинание мгновенно заканчивается.
| **Провал**: Цель получает полный начальный урон и состояние "тошнота 1". Если цель восстанавливается от тошноты, продолжительный урон и заклинание заканчиваются.
| **Критический провал**: Как и провал, но цель получает состояние "тошнота 2".

----------

**Усиление (+1)**: Урон увеличивается на 2d4, а продолжительный урон на 1d4.





.. include:: /helpers/actions.rst