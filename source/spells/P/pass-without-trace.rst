.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--p--Pass-without-Trace:

Бесследный шаг (`Pass without Trace <http://2e.aonprd.com/Spells.aspx?ID=215>`_) / Закл. 1
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- преграждение

**Обычай**: природный

**Использование**: |д-2| жестовый, словесный

**Продолжительность**: 1 час

----------

Вы делаете следы, оставляемые позади, неясными, и затрудняете другим возможность найти вас.
КС проверок Выслеживания, получают бонус состояния +4 или равны вашему КС заклинаний, в зависимости от того, что больше.
Вы единовременно можете получать преимущество только от одного *бесследного шага*.

----------

**Усиление (2-й)**: Продолжительность увеличивается до 8 часов.

**Усиление (4-й)**: Продолжительность увеличивается до 8 часов.
Заклинание имеет дистанцию 20 футов и область 20-футовую эманацию, действуя на 10 существ по вашему выбору в области действия.





.. include:: /helpers/actions.rst