.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--o--Overwhelming-Presence:

Всепоглощающее присутствие (`Overwhelming Presence <https://2e.aonprd.com/Spells.aspx?ID=212>`_) / Закл. 9
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- очарование
- недееспособность
- слуховое
- визуальное
- ментальное

**Обычай**: сакральный, оккультный

**Использование**: |д-2| жестовый, словесный

**Область**: 40-футовый взрыв

**Цели**: любое количество существ

**Спасбросок**: Воля

**Продолжительность**: пока не будет воздано должное в полной мере

----------

Вы окружаете себя сверхъестественным великолепием, кажетесь богом или подобным величественным существом.
Вы сами выбираете аспекты своего нового величественного облика.
Это заставляет цели воздавать вам должное, кланяясь или используя какое-то другое действие в соответствии с вашим внешним видом.
То, сколько раз цель должна это сделать, зависит от результата спасброска.

Воздать должное - это действие с признаком "воздействие" или "перемещение", по выбору существа, отдающему должное.
Существо под воздействием этого эффекта должно воздавать должное вам хотя бы раз в свой ход, если это возможно.
Находясь под воздействием этого заклинания, существо "заворожено" вами и не может использовать против вас враждебные действия.
После этого цель временно иммунна на 1 минуту.

| **Критический успех**: Цель невредима.
| **Успех**: Цель должна воздать должное два раза.
| **Провал**: Цель должна воздать должное шесть раз.
| **Критический провал**: Как провал, и цель должна потратить все свои действия чтобы воздать должное, если это возможно.





.. include:: /helpers/actions.rst