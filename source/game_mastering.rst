.. include:: /helpers/roles.rst
.. _ch10--game-mastering:

10. ГМство (Game Mastering)
==========================================================================================================

.. epigraph::

   *As Game Master, you run each session of Pathfinder, providing the link between the players
   and the world of the game. It’s up to you to set the scene as the player characters battle
   monsters, interact with other people, and explore the world.*

-----------------------------------------------------------------------------



When you take on the role of Game Master, you’ll have
the rewarding job of crafting fun experiences for a group
of your friends. Your responsibilities include...
* Telling the story of the group’s adventures in a compelling and consistent way.
* Fleshing out the world in which the game takes place, emphasizing the fantastical while grounding it enough in the real world to feel believable.
* Entertaining the players and yourself with novel concepts, and rewarding creative ideas with interesting outcomes.
* Preparing for game sessions by building or studying adventures and creating characters and plots.
* Improvising the reactions of nonplayer characters and other forces in the world as the players do unexpected things.
* Making rules decisions to ensure fairness and keep the game moving forward.

This chapter provides the tools you need to shoulder
those responsibilities. The following sections break
down the various components of a campaign, discuss the
different modes of play and how to set DCs for the tasks
the PCs attempt, provide different ways of rewarding
player characters, and describe aspects of the environment
that might affect an adventuring party.


Planning a Campaign
-----------------------

A Pathfinder game is typically structured as a campaign—a
serialized story that focuses on a single party of characters.
A campaign is subdivided into multiple adventures,
smaller stories that involve exploration and interaction
with nonplayer characters. A single adventure represents a
complete story that might be connected to the larger arc of
a campaign. Playing an adventure spans one or more game
sessions—gatherings where the group plays a part of the
adventure over the course of several hours.

A campaign provides the overall structure for your
Pathfinder game. As you prepare for your campaign, you’ll
establish its scope and themes, which you’ll then reinforce
in the adventures and scenes that take place within it.

CAMPAIGN LENGTH
~~~~~~~~~~~~~~~~~~~
The length of a campaign can range from a few sessions
to many years. Two main factors determine campaign
length: how much time you need to complete the story,
and how much time players want to devote to the game.


< ... >


Expected Duration
"""""""""""""""""""

Not every campaign ends at the same point. Some
campaigns go all the way to 20th level, ending after the
player characters attain the height of power and confront
the greatest threats any mortal could face. Others end at
a lower level, after the group takes down a major villain
or solves a crucial problem. And still other campaigns
end when players become unable to attend or decide its a
good time to stop playing.

You should have an end point in mind when you start
a campaign. Still, you have to be flexible, since you’re
telling the story alongside other players, and your initial
expectations for the campaign may be proven incorrect.
When you think you’re heading toward a satisfying
conclusion, it’s useful to check in with the other players.
You might say, “I think we have about two sessions left.
Does that work for everyone? Is there any unfinished
business you want to take care of?” This lets you gauge
whether your assumptions match up with the rest of the
group—and make any necessary adjustments.


THEMES
~~~~~~~~~~~~~~~~~~

The themes you choose for your campaign are what
distinguish it from other campaigns. They include the
major dramatic questions of your story and the repeated
use of certain environments or creatures, and they can
also include embracing a genre beyond traditional high
fantasy. The themes you choose for your campaign also
suggest storyline elements you might use.





.. rst-class:: gm-subchap

Running Modes of Play
--------------------------

.. epigraph::

   *Pathfinder sessions are divided into three different modes of play: encounters, exploration,
   and downtime. Each mode represents different kinds of situations, with specific stakes
   and time scales, and characters can use different sorts of actions and reactions in each.*

-----------------------------------------------------------------------------


Encounters take place in real time or slower, and they
involve direct engagement between players and enemies,
potential allies, or each other. Combat and direct social
interaction usually take place in encounter mode.

Exploration is the connective tissue of an adventure,
and it is used whenever characters are exploring a place
where there’s danger or uncertainty, such as an unfamiliar
city or a dungeon. In exploration mode, characters
aren’t in immediate peril, but they must still be on
their toes. Exploration and encounters are collectively
called adventuring.

When the party isn’t adventuring, the characters are in
downtime. This mode covers most of a normal person’s
life, such as mundane, day-to-day tasks and working
toward long-term goals.


Encounters
~~~~~~~~~~~~~~~~~~~~~~

Encounter mode is the most structured mode of play,
and you’ll mostly be following the rules presented in
Chapter 9 to run this mode. Because you usually call for
initiative during exploration before transitioning into
an encounter, guidelines for initiative order appear on
page 498 in the discussion of exploration mode. Rules
for building combat encounters appear on page 488.

**Stakes**: Moderate to high. Encounters always have
significant stakes, and they are played in a step-by-step
time frame to reflect that.

**Time Scale**: Encounter mode is highly structured and
proceeds in combat rounds for combat encounters,
while other sorts of encounters can have rounds
of any length. In combat, 1 minute consists of 10
rounds, where each combat round is 6 seconds long,
but you might decide a verbal confrontation proceeds
in minute-long or longer rounds to give each speaker
enough time to make a solid point.

**Actions and Reactions**: In combat encounters, each
participant’s turn is broken into discrete actions, and
participants can use reactions when their triggers occur.
Reactions can occur in social situations, though their
triggers are usually more descriptive and less tactical.

CHOOSING ADVERSARIES’ ACTIONS
"""""""""""""""""""""""""""""""""

Players often coordinate and plan to be as efficient as
possible, but their adversaries might not. As the GM,
you’re roleplaying these foes, and you decide their tactics.
Most creatures have a basic grasp of simple tactics like
flanking or focusing on a single target. But you should
remember that they also react based on emotions and
make mistakes—perhaps even more than the player
characters do.


Exploration
~~~~~~~~~~~~~~~~~~~~~~

Exploration mode is intentionally less regimented than
encounters. As a result, during exploration you’ll be making
judgment calls on just about everything that happens.

Fundamentally, exploration is all about rewarding the
PCs for learning about their surroundings. To facilitate
this, it’s especially important to have and convey a clear
mental picture of the group’s surroundings. You’ll be
better able to keep track of where the players are and
describe the sights, sounds, and other sensations of their
adventuring locales. Encourage the players to have their
characters truly explore, and reward their curiosity. The
things they try to do in exploration mode show you what
they’re interested in and what they consider important.
As you play, you’ll get a good feel for the aspects of
exploration that intrigue certain players, and you can add
more of those things to your adventures or emphasize
these points in published adventures.

< ... >

Exploration Activities
""""""""""""""""""""""""""""""""
In exploration mode, each player who wants to do
something beyond just traveling chooses an exploration
activity for their character. The most common activities
are Avoid Notice, Detect Magic, Hustle, and Search,
though there are many options available. While players
usually hew close to these default activities, there’s no
need for them to memorize the exploration activities and
use them exactly. Instead, allow each player to describe
what their character is doing. Then, as the GM, you can
determine which activity applies. This also means you
determine how an activity works if the character’s actions
differ from those on the list.

The following sections discuss exploration activities that
require adjudication from you beyond the guidelines for
players detailed on pages 479–480 of Chapter 9.

Detect Magic
^^^^^^^^^^^^^^^^^^^^^^^^^^

This activity doesn’t enable characters to automatically
find every single magical aura or object during travel.
Hazards that require a minimum proficiency can’t be
found with detect magic, nor can illusions of equal or
higher level than the spell.
When characters find something magical using this
activity, let them know and give them the option to stop
and explore further or continue on. Stopping brings
you into a more roleplay-heavy scene in which players
can search through an area, assess different items, or
otherwise try to figure out the source of the magic and
what it does. Continuing on might cause the group to
miss out on beneficial magic items or trigger a magic trap.


< ... >







Downtime
~~~~~~~~~~~~~~~~~~~~~~

< ... >










.. rst-class:: gm-subchap

Difficulty Classes
--------------------------

.. epigraph::

   *As the Game Master, it’s up to you to set the difficulty classes (DCs) for checks that don’t
   use a predefined DC. The following sections offer advice on how to set appropriate DCs
   and tweak them as needed to feel natural for your story. Picking a simple DC and using a
   level-based DC each work well in certain circumstances, and you can adjust both types of
   DC using the advice on adjusting difficulty.*

-----------------------------------------------------------------------------



Simple DCs
~~~~~~~~~~~~~~~~~~~~

Sometimes you need to quickly set a Difficulty Class. The
easiest method is to select a simple DC from Table 10–4
by estimating which proficiency rank best matches the task
(that rank is usually not required to succeed at the task).
If it’s something pretty much anyone would have a decent
chance at, use the untrained DC. If it would require a degree
of training, use the DC listed for trained, expert, master, or
legendary proficiency, as appropriate to the complexity of the
task. For example, say a PC was trying to uncover the true
history behind a fable. You determine this requires a check
to Recall Knowledge, and that only someone with master
proficiency in Folktale Lore would know the information,
so you’d set the DC at 30—the simple master DC.

Simple DCs work well when you need a DC on the fly
and there’s no level associated with the task. They’re most
useful for skill checks. Because there isn’t much gradation
between the simple DCs, they don’t work as well for
hazards or combatants, where the PCs’ lives are on the line;
you’re better off using level-based DCs for such challenges.


Level-Based DCs
~~~~~~~~~~~~~~~~~~~~~~~~~

When you’re determining a skill DC based on something
that has a level, use Table 10–5 to set the DC. Find the
level of the subject, and assign the corresponding DC. Since
spells use a 1–10 scale, use the Spell Level column for them.

Use these DCs when a PC needs to Identify a Spell or
Recall Knowledge about a creature, attempts to Earn
Income by performing a task of a certain level, and so
on. You can also use the level-based DCs for obstacles
instead of assigning a simple DC. For example, you
might determine that a wall in a high-level dungeon was
constructed of smooth metal and is hard to climb. You
could simply say only someone with master proficiency
could climb it, and use the simple DC of 30.


< ... >


Specific Actions
~~~~~~~~~~~~~~~~~~~~~~

Several parts of this book, most notably Chapter 4: Skills,
state that you as the GM set the DCs for certain checks or
determine other parameters. Here are guidelines for the most
common tasks. Remember that all of these are guidelines,
and you can adjust them as necessary to suit the situation.


Craft
"""""""""""""""

When a character Crafts an item, use the item’s level
to determine the DC, applying the adjustments from
Table 10–6 for the item’s rarity if it’s not common. You
might also apply the easy DC adjustment for an item the
crafter has made before. Repairing an item usually uses the
DC of the item’s level with no adjustments, though you
might adjust the DC to be more difficult for an item of a
higher level than the character can Craft.


< ... >


Recall Knowledge
""""""""""""""""""""

On most topics, you can use simple DCs for checks to Recall
Knowledge. For a check about a specific creature, trap, or
other subject with a level, use a level-based DC (adjusting
for rarity as needed). You might adjust the difficulty down,
maybe even drastically, if the subject is especially notorious
or famed. Knowing simple tales about an infamous
dragon’s exploits, for example, might be incredibly easy for
the dragon’s level, or even just a simple trained DC.
Alternative Skills

Alternative Skills
^^^^^^^^^^^^^^^^^^^

As noted in the action’s description, a character might
attempt to Recall Knowledge using a different skill than
the ones listed as the default options. If the skill is highly
applicable, like using Medicine to identify a medicinal
tonic, you probably don’t need to adjust the DC. If its
relevance is a stretch, adjust the DC upward as described
in Adjusting Difficulty.


As noted in the action’s description, a character might
attempt to Recall Knowledge using a different skill than
the ones listed as the default options. If the skill is highly
applicable, like using Medicine to identify a medicinal
tonic, you probably don’t need to adjust the DC. If its
relevance is a stretch, adjust the DC upward as described
in Adjusting Difficulty.

Additional Knowledge
^^^^^^^^^^^^^^^^^^^^^^^^^

Sometimes a character might want to follow up on a check
to Recall Knowledge, rolling another check to discover
more information. After a success, further uses of Recall
Knowledge can yield more information, but you should
adjust the difficulty to be higher for each attempt. Once a
character has attempted an incredibly hard check or failed
a check, further attempts are fruitless—the character has
recalled everything they know about the subject.


< ... >



SENSE DIRECTION
""""""""""""""""""""""""

Pick the most appropriate simple DC when someone uses
Survival to Sense Direction. This is usually the trained DC
in normal wilderness, expert in deep forest or underground,
master in featureless or tricky locations, or legendary in
weird or surreal environments on other planes.


< ... >





.. rst-class:: gm-subchap

Rewards
--------------------------

.. epigraph::

   *In Pathfinder, player characters can receive three kinds of rewards for their heroic deeds:
   Hero Points, which they can use to get out of sticky situations; Experience Points, which
   they’ll use to level up; and treasure, including powerful magic items.*

-----------------------------------------------------------------------------


< ... >










.. rst-class:: gm-subchap

Environment
--------------------------

.. epigraph::

   *Primarily used during exploration, environment rules bring the locales your party
   travels through to life. You’ll often be able to use common sense to adjudicate how
   environments work, but you’ll need special rules for environments that really stand out.*

-----------------------------------------------------------------------------


Each of the environments presented in this section uses
the terrain rules (which are summarized on page 514
and appear in full beginning on page 475) in different
ways, so be sure to familiarize yourself with those rules
before reading this section. Some environments refer to
the rules for climate (page 517) and natural disasters
(beginning on page 517). Many places have the traits of
multiple environments; a snow-covered mountain might
use both the arctic and mountain environments, for
example. For environmental features with effects based
on how tall or deep they are, those effects vary further
based on a creature’s size. For instance, a shallow bog
for a Medium creature might be a deep bog for smaller
creatures, and a deep bog for a Medium creature could
be only a shallow bog for a larger creature (and so
insignificant for a truly massive creature that it isn’t
even difficult terrain).

Table 10–12 lists the features of various environments
alphabetically for quick reference. The Proficiency DC
Band entry indicates a range of appropriate simple DCs
for that environmental feature, while also providing a
rough estimate of the danger or complexity of the feature.


< ... >





.. rst-class:: gm-subchap

Hazards
--------------------------

.. epigraph::

   *Dungeons are rife with devious traps meant to protect the treasures within. These range
   from mechanical devices that shoot darts or drop heavy blocks to magic runes that
   explode into bursts of flame. In addition to traps, adventurers may stumble into other
   types of hazards, including naturally occurring environmental hazards, mysterious
   hauntings, and more.*

-----------------------------------------------------------------------------



Detecting a Hazard
~~~~~~~~~~~~~~~~~~~~~~

Every hazard has a trigger of some kind that sets its
dangers in motion. For traps, this could be a mechanism
like a trip wire or a pressure plate, while for an
environmental hazard or haunt, the trigger may simply be
proximity. When characters approach a hazard, they have
a chance of finding the trigger area or mechanism before
triggering the hazard. They automatically receive a check
to detect hazards unless the hazards require a minimum
proficiency rank to do so.

During exploration, determine whether the party
detects a hazard when the PCs first enter the general area
in which it appears. If the hazard doesn’t list a minimum
proficiency rank, roll a secret Perception check against
the hazard’s Stealth DC for each PC. For hazards with a
minimum proficiency rank, roll only if someone is actively
searching (using the Search activity while exploring or
the Seek action in an encounter), and only if they have the
listed proficiency rank or higher. Anyone who succeeds
becomes aware of the hazard, and you can describe what
they notice.

Magical hazards that don’t have a minimum proficiency
rank can be found using detect magic, but this spell
doesn’t provide enough information to understand or
disable the hazard—it only reveals the hazard’s presence.
Determining a magical hazard’s properties thoroughly
enough to disable it requires either the use of more
powerful magic or a successful skill check, likely using
Identify Magic or Recall Knowledge. Magical hazards
with a minimum proficiency rank cannot be found with
detect magic at all.




< ... >





Disabling a Hazard
~~~~~~~~~~~~~~~~~~~~~~

The most versatile method for deactivating traps is the
Disable a Device action of the Thievery skill, though
most mechanical traps can also simply be smashed, and
magical traps can usually be counteracted. Environmental
hazards often can be overcome with Nature or Survival,
and haunts can often be overcome with Occultism or
Religion. The specific skill and DC required to disable
a hazard are listed in the hazard’s stat block. As with
detecting a hazard, disabling a hazard might require
a character to have a certain proficiency rank in the
listed skill.


Damaging a Hazard
""""""""""""""""""""

Rather than trying to carefully disable a hazard, a
character might just smash it. Damaging a mechanical
trap or another physical hazard works like damaging
objects: the hazard reduces the damage it takes by its
Hardness. In most cases, hitting the hazard also triggers
it, as explained in Attacking a Hazard below. If a
hazard’s Hit Points are reduced to its Broken Threshold
(BT) or lower, the hazard becomes broken and can’t
be activated, though it can still be repaired. If it’s
reduced to 0 HP, it’s destroyed and can’t be repaired.
(See page 272 in Chapter 6 for more information on
damaging objects.)

< ... >

Attacking a Hazard
^^^^^^^^^^^^^^^^^^^^^^^^^

If someone hits a hazard—especially if it’s a mechanical
trap—they usually trigger it, though you might
determine otherwise in some cases. An attack that breaks
the hazard might prevent it from triggering, depending
on the circumstances. If the hazard has multiple parts,
breaking one part might still trigger the trap. For
example, if a trap has a trip wire in one location and
launches an attack from another location, severing the
trip wire could still trigger the attack. Destroying a
trap in one blow almost never triggers it. These rules
also apply to most damaging spells or other effects in
addition to attacks.

Repairing a Hazard
^^^^^^^^^^^^^^^^^^^^^^^^^

You might allow a character to repair a damaged hazard
to restore its functionality. You determine the specifics of
this, since it can vary by trap. The Repair action might be
insufficient if fixing the trap requires gathering scattered
components or the like. If the item has a Reset entry, the
character needs to do whatever is listed there, in addition
to repairing the damage.



Hazard Format
~~~~~~~~~~~~~~~~

Hazards are presented in a stat block format similar to
those used for monsters. A few notes regarding the format
follow the sample stat block.


.. rst-class:: description

Hazard Name / Hazard [level]
"""""""""""""""""""""""""""""""

- Traits
- Traits
- Traits

**Stealth** This entry lists the Stealth modifier for a complex
hazard’s initiative or the Stealth DC to detect a simple
hazard, followed by the minimum proficiency rank to detect
the hazard (if any) in parentheses. If detect magic can be
used to detect the hazard, this information is located here
as well.

**Description** This explains what the hazard looks like and might
include special rules.

--------------------------

**Disable** The DC of any skill checks required to disable the
hazard are here; if the hazard can be counteracted, its spell
level and counteract DC are listed in parentheses.

**AC** the hazard’s AC; **Saving Throws** the hazard’s saves. Usually
only haunts are subject to Will saves.

**Hardness** the hazard’s Hardness; **HP** the hazard’s Hit Points,
with its Broken Threshold in parentheses; **Immunities** the
hazard’s immunities; **Weaknesses** the hazard’s weaknesses,
if any; **Resistances** the hazard’s resistances, if any

**Action Type** |д-р| or |д-св| This is the reaction or free action the
hazard uses; **Trigger** The trigger that sets off the hazard
appears here. **Effect** For a simple hazard, this effect is often
all the hazard does. For a complex hazard, this might also
cause the hazard to roll initiative.

**Routine** This section describes what a complex hazard does on
each of its turns during an encounter; the number in parentheses
after the word “Routine” indicates how many actions the hazard
can use each turn. Simple hazards don’t have this entry.

**Action** Any action the hazard can use appears here. Typically,
this is a melee or ranged attack.

--------------------------

**Reset** If the hazard can be reset, that information is here.



Level
"""""""""""

The hazard’s level indicates what level of party it’s a good
challenge for. If the hazard involves a toxin, curse, or other
non-spell feature, that feature’s level is the hazard’s level.

Traits
"""""""""""

The most notable hazard traits are trap (constructed to
harm intruders), environmental (natural hazards), and
haunt (spectral phenomena). Traps have a trait to indicate
whether they’re magical or mechanical. Hazards that have
initiative and a routine have the complex trait.

Stealth or Stealth DC
""""""""""""""""""""""""

Complex hazards list their Stealth modifier, which they
use for initiative, instead of their Stealth DC. If you need
the DC, it’s equal to this modifier + 10.



.. rst-class:: gm-hazards

Simple Hazards
~~~~~~~~~~~~~~~~~~~~~~

A simple hazard uses its reaction only once, after which
its threat is over unless the hazard is reset.



Hidden Pit / Hazard 0
"""""""""""""""""""""""""""

- Mechanical
- Trap

**Stealth** DC 18 (or 0 if the trapdoor is disabled or broken)

**Description** A wooden trapdoor covers a pit that’s 10 feet
square and 20 feet deep.

---------------------

**Disable** Thievery DC 12 to remove the trapdoor

**AC** 10; **Fort** +1, **Ref** +1

**Trapdoor Hardness** 3, **Trapdoor HP 12** (BT 6); **Immunities**
critical hits, object immunities, precision damage

**Pitfall** |д-р| **Trigger** A creature walks onto the trapdoor. 
**Effect** The triggering creature falls in and takes falling damage
(typically 10 bludgeoning damage). That creature can use
the Grab an Edge reaction to avoid falling.

---------------------

**Reset** Creatures can still fall into the trap, but the trapdoor
must be reset manually for the trap to become hidden again.



Poisoned Lock / Hazard 1
""""""""""""""""""""""""""""""

- Mechanical
- Trap

**Stealth** DC 17 (trained)

**Description** A spring-loaded, poisoned spine is hidden near
the keyhole of a lock. Disabling or breaking the trap does not
disable or break the lock.

---------------------

**Disable** Thievery DC 17 (trained) on the spring mechanism

**AC** 15; Fort +8, Ref +4

**Hardness** 6, **HP** 24 (BT 12); **Immunities** critical hits, object
immunities, precision damage

**Spring** |д-р| (attack); **Trigger** A creature tries to unlock or Pick the Lock.
**Effect** A spine extends to attack the triggering creature.
**Melee** spine +13, **Damage** 1 piercing plus cladis poison

**Cladis Poison** (poison); **Saving Throw** DC 19 Fortitude; **Maximum
Duration** 4 hours; **Stage 1** 1d6 poison damage and drained 1
(1 hour); **Stage 2** 2d6 poison damage and drained 2 (1 hour);
**Stage 3** 3d6 poison damage and drained 2 (1 hour)




< ... >

Armageddon Orb / Hazard 23
""""""""""""""""""""""""""""

- :rare:`Rare`
- Magical
- Trap

**Stealth** DC 10 or detect magic

**Description** A roiling red orb, forged from a drop of the god
Rovagug’s blood, rains fire from the sky when a specified
condition is met.

---------------------

**Disable** Thievery DC 48 (legendary) to imbue thieves’ tools
with aspects representing Asmodeus and Sarenrae and use
them to drain away the orb’s power over 10 minutes; the
character attempting this check takes 5 fire damage each
round until the orb is depleted

**Burn It All** |д-р| (death, divine, evocation, fire); **Trigger** A special
condition set by the trap’s creator occurs; this is typically
the event of their death. **Effect** Fire rains from the sky in a
100-mile radius, dealing 10d6 fire damage to creatures and
objects in the area. Each creature or object can attempt a DC
46 basic Reflex save. Any creature reduced to 0 Hit Points
by this damage dies instantly. This is not enough damage to
completely burn away a forest or level an entire mountain
or city, but it typically kills most creatures in the area.





.. rst-class:: gm-hazards

Complex Hazards
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Complex hazards function similarly to monsters during
encounters, as they roll initiative and have actions of their
own, though these are usually automated in a routine.



Summoning Rune / Hazard 1
"""""""""""""""""""""""""""""

- Complex
- Magical
- Trap

**Stealth** +7 (trained)

**Description** A cloud of invisible magical sensors in a 10-foot
radius surrounds an invisible wall or floor rune the size of the
creature to be summoned.

---------------------

**Disable** Acrobatics DC 15 to approach without triggering the
trap followed by Thievery DC 17 (trained) to erase the rune, or
dispel magic (1st level; counteract DC 15) to counteract the rune

**Summon Monster** |д-р| (arcane, conjuration, summon; **Trigger**
A creature enters the cloud of magical sensors. **Effect**
This trap summons a specific level 1 creature, determined
when the trap is created. The creature rolls initiative and
remains for 2d6 rounds, after which the spell ends and
the creature disappears. The creature also disappears if
someone disables the trap before the duration expires. The
summoned creature can use 3 actions each round and can
use reactions, unlike most summoned creatures.

---------------------

**Reset** The trap resets each day at dawn.




Drowning Pit / Hazard 3
""""""""""""""""""""""""""""

- Complex
- Mechanical
- Trap

**Stealth** +10 (trained); DC 22 (expert) to notice the water
spouts once the pit opens

**Description** A trapdoor covers a 10-foot-square pit that’s 30
feet deep and has 5 feet of water at the bottom. Four water
spouts in the walls connect to hidden water tanks. Each water
spout extends out of a different wall, 6 inches from the top
of the pit.

---------------------

**Disable** Thievery DC 18 (trained) to seal each water spout,
Thievery DC 22 (trained) to open the trapdoor, or Athletics
DC 22 to Force Open the trapdoor

**AC** 19; **Fort** +8, **Ref** +5

**Trapdoor Hardness** 15, **Trapdoor HP** 60 (BT 30); **Spout
Hardness** 8, **Spout HP** 32 (BT 16); **Immunities** critical hits,
object immunities, precision damage

**Pitfall** |д-р| Trigger A creature walks onto the trapdoor. Effect
The triggering creature falls in and takes damage from the
fall, reduced by 5 feet for falling into the water (typically 12
bludgeoning damage). A creature can Grab an Edge to avoid
falling (page 472). The trapdoor then slams shut, and the
hazard rolls initiative.

---------------------

Routine (4 actions) The trap loses 1 action each turn for each
disabled water spout. On each of the trap’s actions, a spout
pours water, increasing the depth of the water by 5 feet.
Once the pit is full of water, the pit stops using actions, but
creatures in the pit begin drowning (page 478).

---------------------

**Reset** The trap can be reset if the door is manually reengaged
and the water tanks refilled; it can be reset without draining
the pit, but doing so renders it less effective.














.. include:: /helpers/actions.rst