.. include:: /helpers/roles.rst

.. _appendix:

******************************************************************************************
Приложения (Appendix)
******************************************************************************************


.. include:: /appendix/conditions.rst


.. include:: /appendix/glossary_index.rst


.. include:: /appendix/equipment_description.rst



.. include:: /helpers/actions.rst