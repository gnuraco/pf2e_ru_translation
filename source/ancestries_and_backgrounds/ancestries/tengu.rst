.. include:: /helpers/roles.rst

.. rst-class:: ancestry
.. _ancestry--Tengu:

Тэнгу (Tengu)
=============================================================================================================

.. epigraph::
	
	*Тэнгу - общительный и находчивый народ, который распространился далеко от своей прародины в Тянь Ся, собирая и комбинируя все инновации и традиции, с которыми они сталкиваются, с теми, что имеют из своей долгой истории.*

-----------------------------------------------------------------------------

.. rst-class:: sidebar-char-ancestry-class

.. sidebar:: hidden

	.. rubric:: Редкость

	Необычный


	.. rubric:: Очки здоровья

	6


	.. rubric:: Размер

	Средний


	.. rubric:: Скорость

	25 футов


	.. rubric:: Повышения характеристик

	Ловкость

	Свободное


	.. rubric:: Языки

	Всеобщий

	Тэнгу

	Дополнительные языки в количестве вашего модификатора Интеллекта (если положительный).
	Выберите из Дварфский, Эльфийский, Полуросликов, Гномий, Гоблинский, Сильван, и тех к которым у вас есть доступ (распространенные в вашем регионе).


	.. rubric:: Признаки

	Гуманоид

	Тэнгу


	.. rubric:: Сумеречное зрение

	Вы можете видеть при тусклом свете, как если бы это был яркий свет, и вы игнорируете состояние "скрыт" из-за тусклого света.


	.. rubric:: Острый клюв (Sharp Beak)

	С вашим острым клювом, вы никогда не остаетесь без оружия.
	У вас есть безоружная атака клювом, которая наносит 1d6 колющего урона.
	Ваш клюв относятся к группе "драка", и имеет признаки "точное" и "безоружная".


Тэнгу - выживальщики и собеседники, одинаково хорошо живущие в дикой природе и находящие нишу в дремучих городах.
Они накапливают знания, инструменты и компаньонов, добавляя их в свою коллекцию по мере путешествия.

Диаспора тэнгу распространилась по всему Голариону в поисках лучшей жизни, принося свое мастерство создания клинков в земли, далекие от их дома.
В приморских регионах тэнгу особенно часто работают рыбаками, кузнецами и "пожирателями проклятий" - членами экипажей кораблей, которые, как считается, достоверно или нет, поглощают неудачи.
Живя в различных условиях и местах, тэнгу, как правило, никогда не осуждают, особенно в отношении социального положения, хотя их готовность общаться с нарушителями закона часто заставляла некоторых смотреть на них с подозрением.

Если вы хотите сыграть персонажем, имеющим богатую историю ремесел и традиций, но который с радостью подбирает перенимает новые практики, товарищей, слова и предметы по мере необходимости, вам следует сыграть за тэнгу.



.. rst-class:: h3
.. rubric:: Вы можете ...

* Быть общительным и стремиться найти свою собственную стаю
* Жадно впитывать практики тех, кто вас окружает, иногда даже забывая, откуда они взялись
* Быть готовы взять на себя любую задачу или работу, что бы ни думали другие


.. rst-class:: h3
.. rubric:: Другие возможно ...

* Смотрят на вас как на источник эклектических навыков и знаний, особенно связанных с языками
* Смущаются, когда вы одновременно уважаете и пренебрегаете традицией
* С трудом читают выражение вашего лица или относятся к вам с подозрением и суеверием



.. rst-class:: h3
.. rubric:: Физическое описание

У тэнгу много птичьих характеристик.
Их лица имеют острые клювы, а чешуйчатые предплечья и голени заканчиваются когтями.
Поскольку закрытая обувь обычно плохо сидит, если не сделана на заказ, многие тэнгу носят открытые сандалии или просто ходят босиком.
Тэнгу редко достигают более 5 футов в высоту, и они даже легче, чем можно было бы предположить, поскольку у них полые кости.
У небольшого числа тэнгу есть рудиментарные крылья, неспособные к настоящему полету.
Тэнгу вылупляются из яиц и остаются без перьев в течение первого года жизни, в течение которого они редко покидают дом.
Вскоре у них вырастает нежный серый пух, которая сменяется темным покровом взрослых перьев к тому времени, когда они достигают возраста около 15 лет.
Тэнгу используют свои выпавшие перья в различных инструментах, от простых пишущих перьев до волшебных вееров, чтобы сконцентрировать магию своих предков.
Многие тэнгу меняют свой внешний вид, окрашивая узоры на перьях или когтях, что усиливает их язык тела и имеет дополнительное преимущество, помогая другим гуманоидам в понимании их выражений.



.. rst-class:: h3
.. rubric:: Общество

Тэнгу чрезвычайно общительны, объединяясь в обширные сообщества, когда многие семьи живут в соседних домах и разделяют работу по дому.
Дети тэнгу, выросшие в одном районе, считают друг друга братьями и сестрами, обычно забывая, кто из них имеет общую биологическую связь.

Наибольший разрыв в обществе тэнгу наблюдается между теми, кто остался в своей прародине, и теми, кто расселился по миру.
Тэнгу называют эти две группы "на насесте" и "мигрирующие" соответственно.
Тэнгу "на насесте", как правило, более традиционны и консервативны и особенно озабочены сохранением своей культуры перед лицом многолетней разрушения от угнетения.
Мигрирующие тэнгу, с другой стороны, жадно впитывают культуру различных народов и поселений, которые они теперь называют домом.



.. rst-class:: h3
.. rubric:: Мировоззрение и религия

Тэнгу часто являются последователями веры региона, в котором они выросли, хотя божеством предков тэнгу является бог бури Хей Фэн (`Hei Feng <https://2e.aonprd.com/Deities.aspx?ID=40>`_).
Печально известные пьяные кутежи и перепады настроения этого бога заставляют многих тэнгу вместо этого сосредотачивать свое внимание на богах свободы и путешествий, таких как :ref:`Дезна <Deity--Desna>` или :ref:`Кайдэн Кайлин <Deity--Cayden-Cailean>`, или божествах природы, таких как :ref:`Гозрей <Deity--Gozreh>`.
`Бесмара (Besmara) <https://2e.aonprd.com/Deities.aspx?ID=32>`_ также является распространенным предметом поклонения тэнгу, особенно среди тех, которые живут на море.

До своего рассеяния по миру, тэнгу исповедовали синкретическую веру, которая сочетала в себе политеистическое поклонение божествам, ответственным за создание природного мира.
Поскольку фольклор тэнгу утверждает, что тэнгу давным-давно спустился с ночного неба на падающих звездах, чтобы отдохнуть на самых высоких вершинах Голариона, анимистические ритуалы практиковались на горах и других великих природных объектах.
Даже сегодня тэнгу редко разграничивают поклонением божественному и природному.

Тэнгу гораздо больше озабочены балансом между традиционализмом и приспособляемостью, чем добром и злом, причем принципиальные тэнгу чаще встречаются среди "насестов", а хаотичные тэнгу чаще встречаются среди "мигрирующих".



.. rst-class:: h3
.. rubric:: Тэнгу-авантюристы (Tengu Adventurers)

Тэнгу, который покидает родину предков, испытывает сильную тягу к приключениям, пересекает огромные расстояния, собирает прекрасные сокровища и отваживается на вызовы битвы или волнующегося моря.

Предыстория тэнгу может отражать его место на родине или в диаспоре тэнгу.
Это могут быть акробат, бармен, шарлатан, эмиссар, артист, гадалка, азартный игрок, купец, кочевник, моряк, бандит, курьер, мятежник, беженец и мусорщик.

Тэнгу часто становятся плутами, бардами, оракулами, рейнджерами или сорвиголовами.



.. rst-class:: h3
.. rubric:: Имена

Хотя тэнгу "на насесте" склоняются к более традиционным именам с твердыми согласными, часто встречающимися в языке Тэнгу, тенденция мигрирующих тэнгу легко впитывать и адаптировать культуру окружающих, привела к именам, которые сочетают элементы любых языков, подходящих фантазии носителя имени.

.. rst-class:: h4
.. rubric:: Пример имен

Арккак, Чуко, Долра, Дородара, Каккариэль, Кора, Маррак, Моссара, Пуларрка, Рарорель, Рук, Так-Так, Цукотарра






Наследия тэнгу (Tengu Heritages)
-----------------------------------------------------------------------------------------------------------

Как подвижный народ, рассеянный от своих первоначальных домов, на Голарионе есть большое разнообразие тэнгу.
На 1-м уровне выберите одно из следующих наследий тэнгу.


.. _ancestry-heritage--Tengu--Jinxed:

Проклятый (`Jinxed Tengu <https://2e.aonprd.com/Heritages.aspx?ID=77>`_)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Источник**: Advanced Player's Guide pg. 25

Ваш род подвергалось проклятию за проклятием, и теперь они соскальзывают с ваших перьев, как дождь.
Если вы успешно совершаете спасбросок против проклятия или эффекта неудачи, то вместо этого он становится критические успешным.
Когда вы получите состояние "обречен", сделайте чистую проверку с КС 17.
В случае успеха, снизьте величину состояния "обречен" которое вы получите на 1.


.. _ancestry-heritage--Tengu--Mountainkeeper:

Хранитель гор (`Mountainkeeper Tengu <https://2e.aonprd.com/Heritages.aspx?ID=78>`_)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Источник**: Advanced Player's Guide pg. 25

Вы происходите из рода аскетов тэнгу, и это дает вам связь с духами мира и "Великого запределья".
Вы можете по желанию колдовать чары :ref:`spell--d--Disrupt-Undead` как врожденное природное заклинание.
Чары усиливаются до уровня заклинания, равного половине вашего уровня с округлением до большего.
Каждый раз, когда вы колдуете заклинание наследия тэнгу или способности родословной, вы можете решить является ли оно сакральным или природным.


.. _ancestry-heritage--Tengu--Skyborn:

Небесный (`Skyborn Tengu <https://2e.aonprd.com/Heritages.aspx?ID=79>`_)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Источник**: Advanced Player's Guide pg. 26

Ваши кости могут быть особенно легкими, вы можете быть редким тэнгу с крыльями, или ваша связь с духами ветра и неба может быть сильнее, чем у большинства, замедляя ваше падение по воздуху.
Вы не получаете никакого урона от падения, независимо от расстояния, которое вы падаете.


.. _ancestry-heritage--Tengu--Stormtossed:

Штормовой (`Stormtossed Tengu <https://2e.aonprd.com/Heritages.aspx?ID=80>`_)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Источник**: Advanced Player's Guide pg. 26

Будь то благодаря благословению Хей Фэна или вылуплению из яйца во время шквального ветра, вы устойчивы к штормам.
Вы получаете сопротивление электричеству, равное половине вашего уровня (минимум 1).
Вы автоматически преуспеваете в чистых проверках, когда целитесь в скрытое существо, если это скрытие вызвано дождем или туманом.


.. _ancestry-heritage--Tengu--Taloned:

Когтистый (`Taloned Tengu <https://2e.aonprd.com/Heritages.aspx?ID=81>`_)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Источник**: Advanced Player's Guide pg. 26

Ваши когти столь же острые и сильные, как и ваш клюв.
Вы получаете безоружную атаку когтями, которая наносит 1d4 рубящего урона.
Ваши когти относятся к группе "драка", и имеют признаки "быстрое", "точное", "универсальное колющее" и "безоружная".



Универсальные наследия (Versatile Heritages)
-----------------------------------------------------------------------------------------------------------

.. include:: /helpers/versatile_heretages.rst










.. rst-class:: ancestry-class-feats
.. _ancestry--Tengu-Feats:

Способности родословной
-----------------------------------------------------------------------------------------------------------

На 1-м уровне вы получаете одну способность родословной, и получаете дополнительную каждые 4 уровня после этого (на 5-м, 9-м, 13-м и 17-м уровнях).
Как тэнгу, вы выбираете из следующих способностей.


1-й уровень
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. _ancestry-feat--Tengu--Scavengers-Search:

Поиск мусорщика (`Scavenger's Search <https://2e.aonprd.com/Feats.aspx?ID=1310>`_) / 1
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- тэнгу

**Источник**: Advanced Player's Guide pg. 26

----------

Вы всегда в поисках припасов и ценных вещей.
Каждый раз, когда вы используете действие :ref:`action--Seek`, чтобы найти объекты (включая потайные двери и опасности), вы можете искать на свой выбор в 10-футовой эманации вокруг себя, или в области размером 15х15 футов рядом с вами, а не в одной области 10х10 футов рядом.


.. _ancestry-feat--Tengu--Squawk:

Кря! (`Squawk! <https://2e.aonprd.com/Feats.aspx?ID=1311>`_) |д-р| / 1
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- тэнгу

**Триггер**: Вы критически провалили проверку Обмана, Дипломатии или Запугивания против существа не имеющего признак "тэнгу"

**Источник**: Advanced Player's Guide pg. 26

----------

Вы издаете неловкий крик, взъерошиваете перья или изображаете какой-нибудь другим птичий тик, чтобы прикрыть социальную оплошность или бестактность.
Вместо критического провала, вы получаете простой провал спровоцировавшей проверки.
Все существа ставшие свидетелем вашего "Кря!" временно иммунны на 24 часа.


.. _ancestry-feat--Tengu--Storms-Lash:

Удар бури (`Storm's Lash <https://2e.aonprd.com/Feats.aspx?ID=1312>`_) / 1
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- тэнгу

**Источник**: Advanced Player's Guide pg. 26

----------

Ветер и молния всегда были вам близкими друзьями.
Вы можете по желанию колдовать чары :ref:`spell--e--Electric-Arc` как врожденное природное заклинание.
Чары усиливаются до уровня заклинания, равного половине вашего уровня с округлением до большего.


.. _ancestry-feat--Tengu--Lore:

Знания тэнгу (`Tengu Lore <https://2e.aonprd.com/Feats.aspx?ID=1313>`_) / 1
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- тэнгу

**Источник**: Advanced Player's Guide pg. 26

----------

Вы научились навыкам выживания в том месте, где был расселен ваш народ.
Вы становитесь обучены Обществу и Выживанию.
Если вы автоматически станете обучены одному из этих навыков (например, из-за вашей предыстории или класса), то вместо этого становитесь обучены другому навыку на свой выбор.
Вы так же становитесь обучены Знаниям тэнгу.


.. _ancestry-feat--Tengu--Weapon-Familiarity:

Знакомство с оружием тэнгу (`Tengu Weapon Familiarity <https://2e.aonprd.com/Feats.aspx?ID=1314>`_) / 1
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- тэнгу

**Источник**: Advanced Player's Guide pg. 26

----------

Ты тренировался с клинком и другим оружием тэнгу с тех пор, как вылупился.
Вы получаете доступ к хаккарам (khakkara).
Дополнительно, выберите 2 оружия из группы мечей.
Вы можете выбрать из всех обычных воинских мечей, плюс катаны, храмового меча и вакидзаси.
С целью определения вашего уровня мастерства с этим оружием, оно считается простым, а если оружие не обычное (не common), то вы получаете к нему доступ.
Если вы обучены со всем воинским оружием, то вы можете выбирать так же из обычных улучшенных мечей.

Вы так же получаете доступ ко всему необычному оружию тэнгу.
С целью определения вашего уровня мастерства с этим оружием, воинское оружие тэнгу является простым оружием, а улучшенное оружие тэнгу является воинским.





5-й уровень
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. _ancestry-feat--Tengu--Eat-Fortune:

Съесть удачу (`Eat Fortune <https://2e.aonprd.com/Feats.aspx?ID=1315>`_) |д-р| / 5
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- сакральная
- прорицание
- концентрация
- тэнгу

**Триггер**: Существо в пределах 60 футов использует эффект удачи или неудачи

**Частота**: раз в день

**Источник**: Advanced Player's Guide pg. 26

----------

Когда кто-то пытается исказить судьбу, вы поглощаете это вмешательство.
Спровоцировавший эффект прерывается.
Если это эффект неудачи, "Съесть удачу" получает эффект "удача";
если это эффект удачи, "Съесть удачу" получает эффект "неудача".
Эта удача или неудача применяется к тому же броску, что будет у спровоцировавшего эффекта, поэтому вы не можете отменить эффект удачи использованием "Съесть удачу" и потом применить эффект неудачи к этому же броску.


.. _ancestry-feat--Tengu--Long-Nosed-Form:

Длинноносая форма (`Long-Nosed Form <https://2e.aonprd.com/Feats.aspx?ID=1316>`_) |д-1| / 5
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- природная
- превращение
- полиморф
- концентрация
- тэнгу

**Источник**: Advanced Player's Guide pg. 26

----------

Вы можете трансформироваться в специфичную, любопытно выглядящую человеческую форму.
Эта человеческая форма имеет тот же возраст и тип телосложения, что и ваша форма тэнгу, и имеет примерно такие же физические черты, такие как рост, хотя ваш нос остается таким же длинным, как ваш клюв, а цвет лица имеет красный оттенок, независимо от цвета кожи вашей человеческой формы.
Использование "Длинноносой формы" считается созданием маскировки для использования действия :ref:`skill--Deception--Impersonate` навыка Обмана.
Из-за вашей несовершенной трансформации, она автоматически не преодолевает КС Восприятия, чтобы определить, являетесь ли вы человеком, хотя вы можете объяснить или скрыть свои черты тэнгу.
В человеческой форме вы теряете свой безоружный :ref:`action--Strike` клювом, а так же любые другие безоружные Удары, которые вы получили от наследия тэнгу или способности родословной.
Вы можете сколь угодно долго оставаться в своей человеческой форме, и можете вернуться в форму тэнгу снова использовав это действие.


.. _ancestry-feat--Tengu--One-Toed-Hop:

Однопальцевый прыжок (`One-Toed Hop <https://2e.aonprd.com/Feats.aspx?ID=1317>`_) |д-1| / 5
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- тэнгу

**Источник**: Advanced Player's Guide pg. 27

----------

Приняв своеобразную стойку, вы делаете короткий прыжок на каждом пальце ноги.
Вы делаете вертикальный :ref:`action--Leap` не провоцируя реакции, которые могут быть спровоцированы действиями с признаком "движение" или при выходе или входе в квадрат.


.. _ancestry-feat--Tengu--Weapon-Study:

Изучение оружия тэнгу (`Tengu Weapon Study <https://2e.aonprd.com/Feats.aspx?ID=1318>`_) / 5
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- тэнгу

**Предварительные условия**: :ref:`ancestry-feat--Tengu--Weapon-Familiarity`

**Источник**: Advanced Player's Guide pg. 27

----------

Вы научились приемам использования клинков и другого оружия тэнгу.
Когда вы критически попадаете используя оружие тэнгу, или одним из перечисленных в :ref:`ancestry-feat--Tengu--Weapon-Familiarity`, вы применяете критический эффект специализации оружия.





9-й уровень
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. _ancestry-feat--Tengu--Eclectic-Sword-Training:

Эклектическая тренировка с мечем (`Eclectic Sword Training <https://2e.aonprd.com/Feats.aspx?ID=1319>`_) / 9
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- тэнгу

**Предварительные условия**: :ref:`ancestry-feat--Tengu--Weapon-Familiarity`

**Источник**: Advanced Player's Guide pg. 27

----------

Вас всегда учили, что нужно уметь использовать любое оружие, которое попадется на вашем пути.
Вы можете заменить любой из мечей, указанных в вашем :ref:`ancestry-feat--Tengu--Weapon-Familiarity`, на другие мечи, соответствующие тем же характеристикам.
Чтобы выбрать этот новый меч, вам надо попрактиковаться с ним во время своих ежедневных приготовлений, и этот новый выбор длится только до ваших следующих приготовлений.
Это меняет только ваш уровень мастерства; это не меняет ваш доступ к оружию.


.. _ancestry-feat--Tengu--Soaring-Flight:

Парящий полет (`Soaring Flight <https://2e.aonprd.com/Feats.aspx?ID=1320>`_) |д-2| / 9
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- природная
- превращение
- трансформация
- тэнгу

**Предварительные условия**: наследие :ref:`ancestry-heritage--Tengu--Skyborn`

**Частота**: раз в день

**Источник**: Advanced Player's Guide pg. 27

----------

Быть тэнгу - значит не обременять себя заботами о мире под ногами.
Вы отращиваете пару магических крыльев, чтобы удлинить свои собственные.
На 5 минут, вы получаете Скорость полета, равную вашей наземной Скорости или 20 футам, в зависимости от того, что больше.





13-й уровень
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. _ancestry-feat--Tengu--Weapon-Expertise:

Опыт с оружием тэнгу (`Tengu Weapon Expertise <https://2e.aonprd.com/Feats.aspx?ID=1321>`_) / 13
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- тэнгу

**Предварительные условия**: :ref:`ancestry-feat--Tengu--Weapon-Familiarity`

**Источник**: Advanced Player's Guide pg. 27

----------

Обучение сделало вас экспертом в оружии тэнгу.
Всякий раз, когда вы получаете особенность класса, которая дает вам экспертное или выше мастерство владения определенным оружием, вы так же получаете его для всего оружия, с которым вы обучены от :ref:`ancestry-feat--Tengu--Weapon-Familiarity`





17-й уровень
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. _ancestry-feat--Tengu--Great-Tengu-Form:

Форма великого тэнгу (`Great Tengu Form <https://2e.aonprd.com/Feats.aspx?ID=1322>`_) / 17
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- тэнгу

**Предварительные условия**: :ref:`ancestry-feat--Tengu--Long-Nosed-Form`

**Источник**: Advanced Player's Guide pg. 27

----------

Вы принимаете внушительную форму большого крылатого они.
Раз в день, как часть использования :ref:`ancestry-feat--Tengu--Long-Nosed-Form`, вы так же получаете преимущества заклинаний :ref:`spell--e--Enlarge` 4-го уровня и :ref:`spell--f--Fly`.
Это длится 5 минут или пока вы не измените форму обратно на обычную, в зависимости от того, что случится раньше.





.. include:: /helpers/actions.rst