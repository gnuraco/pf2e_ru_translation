.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--i--Ill-Omen:

Дурная примета (`Ill Omen <https://2e.aonprd.com/Spells.aspx?ID=578>`_) / Закл. 1
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- прорицание
- неудача
- проклятие

**Обычай**: оккультный

**Использование**: |д-2| жестовый, словесный

**Дистанция**: 30 футов

**Цели**: 1 существо

**Спасбросок**: Воля

**Продолжительность**: 1 раунд

**Источник**: Lost Omens: Gods & Magic pg. 108 / Advanced Player's Guide pg. 221

----------

Цель поражается невезением, которое сбивает ее с толку.
Цель должна сделать спасбросок Воли.

| **Успех**: Цель невредима
| **Провал**: Первый раз в течение продолжительности заклинания, когда цель делает бросок атаки или проверку навыка, она должна бросить дважды и использовать худший результат
| **Критический провал**: Каждый раз в течение продолжительности заклинания, когда цель делает бросок на атаку или проверку навыка, она должна бросить дважды и использовать худший результат.





.. include:: /helpers/actions.rst