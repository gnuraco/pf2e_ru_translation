.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--c--Charm:

Очаровать (`Charm <http://2e.aonprd.com/Spells.aspx?ID=34>`_) / Закл. 1
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- очарование
- недееспособность
- эмоция
- ментальное

**Обычай**: арканный, оккультный, природный

**Использование**: |д-2| жестовый, словесный

**Дистанция**: 30 футов

**Цели**: 1 существо

**Спасбросок**: Воля

**Продолжительность**: 1 час

----------

Для цели, твои слова - словно бальзам, а ваш лик будто окутан мечтательной мглой.
Оно должно пройти спасбросок Воли, с бонусом обстоятельства +4, если вы или ваш союзник недавно угрожали ему, или использовали против него враждебные действия.

Вы можете :ref:`action--Dismiss` заклинание.
Если вы используете враждебные действия против цели, заклинание заканчивается.
Когда заклинание заканчивается, цель не обязательно понимает, что она была очарована, если ее дружба с вами или действия, которые вы убедили ее предпринять, не противоречат ее ожиданиям, то есть вы потенциально можете убедить цель оставаться вашим другом с помощью мирских способов.

| **Критический успех**: Заклинание не действует на цель, и она понимает что вы пытались ее очаровать.
| **Успех**: Заклинание не действует на цель, но она думает, что это было что-то безвредное, а не *очарование*, если только она не идентифицирует его (с помощью :ref:`ch7--Identifying-Spells`).
| **Провал**: Отношение цели к вам становится дружественным. Если она уже была дружественной, то становится любезной. Она не может использовать враждебные действия против вас.
| **Критический провал**: Отношение цели к вам становится любезным, и она не может использовать враждебные действия против вас.

.. versionchanged:: /errata-r1
	Изменена ссылка на страницу по "Определению магии".

----------

**Усиление (4-й)**: Продолжительность длится до вашего следующего дневного приготовления.

**Усиление (8-й)**: Продолжительность длится до вашего следующего дневного приготовления, и количество целей увеличивается до 10.





.. include:: /helpers/actions.rst