.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--b--Blade-Barrier:

Стена лезвий (`Blade Barrier <https://2e.aonprd.com/Spells.aspx?ID=24>`_) / Закл. 6
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- разрушение
- сила

**Обычай**: сакральный

**Использование**: |д-3| жестовый, словесный, материальный

**Дистанция**: 120 футов

**Продолжительность**: 1 минута

----------

Лезвия из силы формируют стену, которая мешает.
Стена появляется прямой линией, высотой 20 футов, 120 футов в длину, и 2 фута в толщину, и она дает укрытие.
Стена наносит 7d8 урона силой каждому существу, которое находится в пространстве стены в момент ее создания, которое пытается пройти через стену, или которое заканчивается ход внутри стены.
Простой спасбросок Рефлекса снижает урон.
Существо, которое успешно сделало спасбросок при создании стены, выталкивается в ближайшее пространство на одну из сторон стены, по его выбору.
Существа, пытающиеся пройти через стену, заканчивают движение рядом со стеной, если крит.проваливают спасброс.

----------

**Усиление (+1)**: Урон увеличивается на 1d8.





.. include:: /helpers/actions.rst