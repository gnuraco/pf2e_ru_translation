.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--t--Tree-Stride:

Древесный переход (`Tree Stride <https://2e.aonprd.com/Spells.aspx?ID=343>`_) / Закл. 5
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- :uncommon:`необычное`
- воплощение
- телепортация
- растение

**Обычай**: природный

**Использование**: 1 минута (жестовый, словесный, материальный)

----------

Вы входите в живое дерево с достаточно большим стволом, чтобы вы поместились внутри, и мгновенно телепортируетесь к любому дереву того же вида в радиусе 5 миль, которое также имеет достаточно большой ствол.
Как только вы входите в первое дерево, вы сразу же узнаете примерное расположение других достаточно больших деревьев того же вида в пределах досягаемости, и если хотите, можете выйти из исходного дерева.
Вы не можете переносить с собой внепространственное пространство; если вы пытаетесь это сделать, то заклинание проваливается.

----------

**Усиление (6-й)**: Выходное дерево может быть на расстоянии вплоть до 50 миль.

**Усиление (8-й)**: Выходное дерево может быть на расстоянии вплоть до 500 миль.

**Усиление (9-й)**: Выходное дерево может быть где угодно на той же планете.





.. include:: /helpers/actions.rst