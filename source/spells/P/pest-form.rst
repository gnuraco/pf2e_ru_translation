.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--p--Pest-Form:

Форма вредителя (`Pest Form <http://2e.aonprd.com/Spells.aspx?ID=217>`_) / Закл. 1
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- превращение
- полиморф

**Обычай**: арканный, природный

**Использование**: |д-2| жестовый, словесный

**Продолжительность**: 10 минут

----------

Вы превращаетесь в боевую форму животного Крошечного размера, такого как кот, насекомое, ящерица или крыса.
Вы можете выбрать конкретный вид животного (например крысу или богомола), но это не влияет на размер формы и показатели.
Когда вы в форме вы получаете признак "животное".
Вы можете Развеять заклинание.

Вы получаете следующие способности:

* КБ = 15 + ваш уровень. Игнорируйте ваши штрафы брони для проверок и снижение Скорости.
* Скорость 10 футов.
* Слабость 5 к физическому урону. (Если вы получаете физический урон в этой форме, то получаете 5 дополнительного урона)
* Сумеречное зрение и неточный нюх 30 футов.
* Модификаторы Акробатики и Скрытности равны +10, если только ваш собственный не выше; модификатор Атлетики -4.

----------

**Усиление (4-й)**: Вы можете превратиться в летающее существо, как птица, что дает вам Скорость полета 20 футов.





.. include:: /helpers/actions.rst