.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--m--Mind-Reading:

Чтение разума (`Mind Reading <http://2e.aonprd.com/Spells.aspx?ID=194>`_) / Закл. 3
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- :uncommon:`необычное`
- прорицание
- обнаружение
- ментальное

**Обычай**: арканный, оккультный

**Использование**: |д-2| жестовый, словесный

**Дистанция**: 30 футов

**Цели**: 1 существо

**Спасбросок**: Воля

**Продолжительность**: 1 раунд или поддерживаемое до 1 минуты

----------

Беглым ментальным прикосновением вы пытаетесь прочитать мысли цели.
Она должна пройти спасбросок Воли.
После этого цель становится иммунной для вашего *чтения разума* на 1 час.

| **Критический успех**: Цель чувствует смутные поверхностные мысли от вас, когда вы колдуете это заклинание.
| **Успех**: Вы узнаете, является ли модификатор Интеллекта цели больше вашего, равен ему или ниже.
| **Провал**: Вы чувствуете смутные поверхностные мысли от цели, когда колдуете это заклинание и вы узнаете, является ли модификатор Интеллекта цели больше вашего, равен ему или ниже.
| **Критический провал**: Как провал, и на протяжении длительности заклинания вы можете поддерживать его чтобы снова обнаружить поверхностные мысли цели. Цель не предпринимает никаких дополнительных спасбросков.





.. include:: /helpers/actions.rst