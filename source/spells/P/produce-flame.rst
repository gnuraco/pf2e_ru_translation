.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--p--Produce-Flame:

Сотворить пламя (`Produce Flame <http://2e.aonprd.com/Spells.aspx?ID=236>`_) / Чары 1
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- чары
- разрушение
- атака
- огонь

**Обычай**: арканный, природный

**Использование**: |д-2| жестовый, словесный

**Дистанция**: 30 футов

**Цели**: 1 существо

----------

Небольшой огненный шар появляется на вашей ладони, и вы бросаете его на расстоянии, либо атакуете в ближнем бою.
Совершите атаку заклинанием против КС цели.
Обычно это дистанционная атака, но вы так же можете сделать атаку в ближнем бою, по существу находящемуся в досягаемости вашей безоружной атаки.
В случае успеха, вы наносите 1d4 плюс ваш модификатор характеристики колдовства огненного урона.
При критическом успехе, цель получает двойной урон и 1d4 продолжительного огненного урона.

----------

**Усиление (+1)**: Увеличиваете урон на 1d4 и продолжительный урон при критическом попадании на 1d4.





.. include:: /helpers/actions.rst