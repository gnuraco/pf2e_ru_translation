.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--n--Negate-Aroma:

Подавить запах (`Negate Aroma <http://2e.aonprd.com/Spells.aspx?ID=206>`_) / Закл. 1
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- преграждение

**Обычай**: арканный, природный

**Использование**: |д-2| жестовый, словесный

**Дистанция**: касание

**Цели**: 1 готовое существо

**Продолжительность**: 1 час

----------

Цель теряет свой запах, не позволяя существам пассивно замечать ее присутствие только через запах, даже если у существ есть точный или неточный запах.
Существо, совершающее проверку Восприятия для Поиска с помощью запаха и других чувств, может заметить отсутствие натурального запаха.
Если у цели есть любые способности, которые зависят от ее запаха, как непреодолимый запах, то они тоже подавляются.

----------

**Усиление (5-й)**: Дистанция заклинания увеличивается до 30 футов, и вы можете выбрать вплоть до 10 целей.





.. include:: /helpers/actions.rst