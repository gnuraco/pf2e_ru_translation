.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--c--Chilling-Spray:

Леденящие брызги (`Chilling Spray <https://2e.aonprd.com/Spells.aspx?ID=571>`_) / Закл. 1
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- разрушение
- холод

**Обычай**: арканный, природный

**Использование**: |д-2| жестовый, словесный

**Область**: 15-футовый конус

**Спасбросок**: Рефлекс

**Источник**: Lost Omens: Gods & Magic pg. 107 / Advanced Player's Guide pg. 217

----------

Конус ледяных осколков вырывается из ваших раскинутых рук и покрывает цель слоем инея.
Вы наносите 2d4 урона холодом существам в области; они должны сделать спасбросок Рефлекса.

| **Критический успех**: Существо невредимо
| **Успех**: Существо получает половину урона
| **Провал**: Существо получает полный урон и штраф состояния -5 футов к Скоростям на 2 раунда
| **Критический провал**: Существо получает двойной урон и штраф состояния -10 футов к Скоростям на 2 раунда

----------

**Усиление (+1)**: Урон увеличивается на 2d4.





.. include:: /helpers/actions.rst