.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--w--Wail-of-the-Banshee:

Вой баньши (`Wail of the Banshee <https://2e.aonprd.com/Spells.aspx?ID=361>`_) / Закл. 9
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- некромантия
- смерть
- слуховое
- негативное

**Обычай**: сакральный, оккультный

**Использование**: |д-2| жестовый, словесный

**Область**: 40-футовая эманация

**Цели**: любое количество существ

**Спасбросок**: Стойкость

----------

Ваш крик холодит души врагов, которые его слышат.
Каждый живой враг в области получает 8d10 негативного урона и должен сделать спасбросок Стойкости.

| **Критический успех**: Цель невредима.
| **Успех**: Цель получает полный урон.
| **Провал**: Цель получает полный урон и состояние "истощен 1d4".
| **Критический провал**: Цель получает двойной урон и состояние "истощен 4".





.. include:: /helpers/actions.rst