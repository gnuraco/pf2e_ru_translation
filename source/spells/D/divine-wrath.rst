.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--d--Divine-Wrath:

Божественный гнев (`Divine Wrath <http://2e.aonprd.com/Spells.aspx?ID=86>`_) / Закл. 4
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- разрушение

**Обычай**: сакральный

**Использование**: |д-2| жестовый, словесный

**Дистанция**: 120 футов

**Область**: 20-футовый взрыв

**Спасбросок**: Стойкость

----------

Вы можете направить ярость вашего божества против врагов противоположного мировоззрения.
Выберите компонент мировоззрение который есть у вашего божества (хаос, зло, добро или принципиальность).
Вы не можете колдовать это заклинание если у вас нет божества или оно нейтрально.
Это заклинание получат признак выбранного мировоззрения.
Вы наносите 4d10 урона выбранным мировоззрением; каждое существо в области должно пройти спасбросок Стойкости.
Существа, соответствующие выбранному мировоззрению невредимы для эффекта.
Те, кто ни соответствуют, ни противоположен мировоззрению, считают результат спасброска на одну ступень выше.

| **Критический успех**: Цель невредима.
| **Успех**: Существо получает половину урона.
| **Провал**: Существо получает полный урон и состояние "тошнота 1".
| **Критический провал**: Существо получает двойной урон и состояние "тошнота 2"; пока его тошнит, оно так же "замедлено 1".

----------

**Усиление (+1)**: Урон увеличивается на 1d10.





.. include:: /helpers/actions.rst