.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--s--Sound-Burst:

Взрыв звука (`Sound Burst <http://2e.aonprd.com/Spells.aspx?ID=292>`_) / Закл. 2
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- разрушение
- звук

**Обычай**: сакральный, оккультный

**Использование**: |д-2| жестовый, словесный

**Дистанция**: 30 футов

**Область**: 10-футовый взрыв

**Спасбросок**: Стойкость

----------

Раздается какофонический шум, наносящий 2d10 урона звуком.
Каждое существо в области должно пройти спасбросок Стойкости.

| **Критический успех**: Существо невредимо.
| **Успех**: Существо получает половину урона.
| **Провал**: Существо получает полный урон и "глухое" на 1 раунд.
| **Критический провал**: Существо получает двойной урон, "глухое" на 1 минуту, и "ошеломлено" на 1 минуту.

.. versionchanged:: /errata-r1
	Изменен эффект крит.провала. Было "глухое и ошеломлено 1 на 1 минуту", что не имело смысла.

----------

**Усиление (+1)**: Урон увеличивается на 1d10.





.. include:: /helpers/actions.rst