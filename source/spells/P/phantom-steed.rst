.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--p--Phantom-Steed:

Фантомный скакун (`Phantom Steed <http://2e.aonprd.com/Spells.aspx?ID=221>`_) / Закл. 2
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- воплощение

**Обычай**: арканный, оккультный, природный

**Использование**: 10 минут (жестовый, словесный)

**Дистанция**: 30 футов

**Продолжительность**: 8 часов

----------

Вы создаете волшебное лошадиное существо, большого размера, на котором можете ездить только вы (или среднего или маленького размера, на ваш выбор).
Лошадь явно призрачная по своей сути, имеет КБ 20 и 10 Очков Здоровья, и автоматически проваливает все спасброски.
Если ее ОЗ снижаются до 0, она исчезает и заклинание заканчивается.
Скакун имеет Скорость 40 футов, и может удерживать вес ездока плюс 20 массы.

----------

**Усиление (4-й)**: Скакун имеет Скорость 60 футов, может ходит по воде, и игнорирует области естественной сложной местности.

**Усиление (5-й)**: Скакун имеет Скорость 60 футов, может ходит по воде, и игнорирует области естественной сложной местности.
Он может так же использовать *хождение по воздуху (air walk)*, но должен закончить ход на твердой поверхности, или упасть.

**Усиление (6-й)**: Скакун может ходить или летать со Скоростью 80 футов, может ходит по воде, и игнорирует естественную сложную местность.





.. include:: /helpers/actions.rst