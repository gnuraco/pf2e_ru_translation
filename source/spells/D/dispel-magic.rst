.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--d--Dispel-Magic:

Рассеять магию (`Dispel Magic <http://2e.aonprd.com/Spells.aspx?ID=78>`_) / Закл. 2
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- преграждение

**Обычай**: арканный, сакральный, оккультный, природный

**Использование**: |д-2| жестовый, словесный

**Дистанция**: 120 футов

**Цели**: 1 эффект заклинания или ничейный предмет

----------

Вы развеиваете магию, поддерживающую заклинание или эффект.
Сделайте проверку :ref:`ch9--Counteracting` против цели.
Если вы успешно прошли проверку против эффекта заклинания, то вы противодействуете ему.
Если вы успешно прошли проверку против предмета, он становится обычным предметом своего типа на 10 минут.
Это не изменяет немагические свойства предмета.
Если цель - артефакт или подобный предмет, вы автоматически проваливаете спасбросок.





.. include:: /helpers/actions.rst