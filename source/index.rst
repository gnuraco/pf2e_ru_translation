.. Pathfinder 2 Core Rulebook documentation master file, created by
   sphinx-quickstart on Wed Sep 18 11:06:43 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Pathfinder 2e
======================================================

.. toctree::
   :maxdepth: 2
   
   introduction
   ancestries_and_backgrounds/index
   classes/index
   skills
   feats
   equipment
   spells/index
   the_age_of_lost_omens
   playing_the_game
   game_mastering
   crafting_and_treasure/index
   appendix



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
