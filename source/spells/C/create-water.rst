.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--c--Create-Water:

Создание воды (`Create Water <http://2e.aonprd.com/Spells.aspx?ID=53>`_) / Закл. 1
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- воплощение
- вода

**Обычай**: арканный, сакральный, природный

**Использование**: |д-2| жестовый, словесный

**Дистанция**: 0 футов

----------

Когда вы складываете руки чашечкой, из них течет вода.
Вы создаете 2 галлона воды.
Если ее никто не пьет, она испаряется через 1 день.





.. include:: /helpers/actions.rst