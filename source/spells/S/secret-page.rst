.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--s--Secret-Page:

Тайная страница (`Secret Page <http://2e.aonprd.com/Spells.aspx?ID=270>`_) / Закл. 3
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- иллюзия
- визуальное

**Обычай**: арканный, оккультный

**Использование**: 1 минута (жестовый, словесный, материальный)

**Дистанция**: касание

**Цели**: 1 страница размером вплоть до 3 квадратных футов

**Продолжительность**: неограниченно

----------

Вы меняете текст цели на абсолютно другой.
Если это текст книги заклинаний или свитка, вы можете изменить чтобы он показывал известное вам заклинание, уровнем *тайной страницы* или ниже.
Заменяющее заклинание нельзя колдовать или применить для подготовки заклинания.
Вы также можете изменить текст в другой, который вы написали или к которому имеете доступ.
Вы можете указать пароль, который позволяет существу, касающемуся страницы изменить текст туда и обратно.
Вы должны выбрать текст для замены и пароль, если хотите, когда произносите заклинание.





.. include:: /helpers/actions.rst