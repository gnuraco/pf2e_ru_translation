.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--r--Raise-Dead:

Вернуть к жизни (`Raise Dead <http://2e.aonprd.com/Spells.aspx?ID=243>`_) / Закл. 6
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- :uncommon:`необычное`
- некромантия
- исцеление

**Обычай**: сакральный

**Использование**: 10 минут (жестовый, словесный, материальный)

**Стоимость**: алмазы на сумму уровня цели (мин. 1) x 200 зм

**Дистанция**: 10 футов

**Цели**: 1 мертвое существо 13-го уровня или ниже

----------

Вы пытаетесь вызвать душу мертвого существа, что требует присутствия относительно неповрежденного тела существа.
Существо должно было умереть в течение последних 3 дней.
Если Фаразма решила, что время существа пришло (по решению Мастера), или если существо не желает вернуться к жизни, это заклинание автоматически проваливается, но алмазы не тратятся при колдовстве.

Если заклинание прошло успешно, существо возвращается к жизни с 1 ОЗ, без подготовленных заклинаний или доступных слотов заклинаний, без очков фокусировки или других ежедневных ресурсов и с любыми долгосрочными ослаблениями старого тела.
Время, проведенное в Могильнике делает цель временно ослабленной, давая ей состояния "неуклюжесть 2", "истощен 2" и "ослаблен 2" на 1 неделю; эти состояния нельзя убрать или снизить никаким способом, пока не пройдет неделя.
Из-за времени, проведенного в загробной жизни, существо также меняется насовсем, например, происходит небольшое изменение личности, появляется белая прядь в волосах или новое странное родимое пятно.

----------

**Усиление (7-й)**: Максимальный уровень цели увеличивается до 15.
Стоимость увеличивается до уровень цели (мин. 1) x 400 зм.

**Усиление (8-й)**: Максимальный уровень цели увеличивается до 17.
Стоимость увеличивается до уровень цели (мин. 1) x 800 зм.

**Усиление (9-й)**: Максимальный уровень цели увеличивается до 19.
Стоимость увеличивается до уровень цели (мин. 1) x 1600 зм.

**Усиление (10-й)**: Максимальный уровень цели увеличивается до 21.
Стоимость увеличивается до уровень цели (мин. 1) x 3200 зм.





.. include:: /helpers/actions.rst