.. toctree::
   :maxdepth: 1
   
   /ancestries_and_backgrounds/versatile_heritages/changeling
   /ancestries_and_backgrounds/versatile_heritages/dhampir
   /ancestries_and_backgrounds/versatile_heritages/aasimar
   /ancestries_and_backgrounds/versatile_heritages/duskwalker
   /ancestries_and_backgrounds/versatile_heritages/tiefling