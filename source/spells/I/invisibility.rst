.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--i--Invisibility:

Невидимость (`Invisibility <http://2e.aonprd.com/Spells.aspx?ID=164>`_) / Закл. 2
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- иллюзия

**Обычай**: арканный, оккультный

**Использование**: |д-2| жестовый, словесный

**Дистанция**: касание

**Цели**: 1 существо

**Продолжительность**: 10 минут

----------

Покрытая иллюзиями, цель становится невидимой.
Это делает ее "необнаруженной" для всех существ, однако существа могут попытаться найти ее, сделав тем самым "спрятанной" для себя (см. ":ref:`ch9--Detecting-Creatures`").
Если цель использует враждебное действие, заклинание заканчивается после завершения враждебного действия.

----------

**Усиление (4-й)**: Заклинание длится 1 минуту, но не заканчивается если цель использует враждебное действие.





.. include:: /helpers/actions.rst