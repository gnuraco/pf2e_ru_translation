.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--s--Speak-with-Plants:

Разговор с растениями (`Speak with Plants <http://2e.aonprd.com/Spells.aspx?ID=294>`_) / Закл. 4
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- прорицание
- растение

**Обычай**: природный

**Использование**: |д-2| жестовый, словесный

**Продолжительность**: 10 минут

----------

Вы можете задавать вопросы и получать ответы от растений, но заклинание не делает их дружелюбнее или умнее, чем обычно.
Большинство нормальных растений имеют особый взгляд на окружающий мир, поэтому они не распознают детали о существах или знают что-либо о мире за пределами их непосредственной окрестности.
Хитрые растительные монстры, скорее всего, будут немногословны и уклончивы, в то время как менее умные часто говорят что-то бессмысленное.





.. include:: /helpers/actions.rst