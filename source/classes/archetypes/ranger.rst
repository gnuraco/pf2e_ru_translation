.. include:: /helpers/roles.rst

.. rst-class:: archetype multiclass
.. _archetype--Ranger:

Рейнджер (`Ranger (Archetype) <https://2e.aonprd.com/Archetypes.aspx?ID=9>`_)
-------------------------------------------------------------------------------------------------------------

Вы научились охотиться, выслеживать и выживать в дикой местности, добавляя навыки рейнджера в свой арсенал.


Мультиклассовые персонажи рейнджеры (Multiclass Ranger Characters)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Архетип рейнджера имеет доступ к отличным вариантам для улучшения знаний персонажа о монстрах и навыках выживания, но рейнджер особенно интересен любому персонажу, желающему стать лучником.

* Алхимики-рейнджеры могут добавить ловушки к их ежедневным бесплатным предметам, получая преимущества от большого модификатора Ремесла, а Интеллект делает их исключительными в навыках со знаниями о монстрах. Архетип рейнджера так же полезен для бомбардиров, которые хотят альтернативы в качестве атак с луком.
* Чемпионы-рейнджеры особенно подходят божествам, которые тематически связаны с природой или имеют лук в виде их предпочитаемого оружия (или и то и другое, в случае с Эрастилом).
* Воины-рейнджеры среди большинства - самые устрашающие лучники, совмещающие лучшие способности лучников из обоих классов, для создания боевого стиля уникального для каждого воина-рейнджера.
* Из плутов-рейнджеров получаются отличные снайперы или бойцы дальнего боя, а силки и ловушки позволяют им, для разнообразия, быть по другую их сторону.
* Заклинатели-рейнджеры получают отличные преимущества при поддержке способностей рейнджера, подкрепляя свои заклинания дистанционными атаками из оружия. Среди всех заклинателей, друиды наиболее сильно тематически пересекаются с рейнджерами, позволяя вам создавать настоящего мастера дикой природы.


.. _arch-feat--Ranger--Dedication:

Посвящение рейнджера (`Ranger Dedication <https://2e.aonprd.com/Feats.aspx?ID=722>`_) / 2
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- архетип
- посвящение
- мультикласс

**Предварительные условия**: Ловкость 14

----------

Вы можете использовать действие :ref:`class-feature--ranger--Hunt-Prey`.
Вы становитесь обучены Выживанию; если вы уже обучены Выживанию, то можете стать обученным в другом навыке на ваш выбор.
Вы становитесь обучены КС класса рейнджера.

**Особенность**: Вы не можете выбрать другую способность посвящения, пока не получите 2 другие способности из архетипа рейнджера.


.. _arch-feat--Ranger--Basic Hunters-Trick:

Простой охотничий трюк (`Basic Hunter's Trick <https://2e.aonprd.com/Feats.aspx?ID=723>`_) / 4
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- архетип

**Предварительные условия**: :ref:`arch-feat--Ranger--Dedication`

----------

Вы получаете способность рейнджера 1-го или 2-го уровня.


.. _arch-feat--Ranger--Resiliency:

Рейнджерская стойкость (`Ranger Resiliency <https://2e.aonprd.com/Feats.aspx?ID=724>`_) / 4
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- архетип

**Предварительные условия**: :ref:`arch-feat--Ranger--Dedication`, класс дающий ОЗ за уровень не более 8 + модификатор Телосложения

----------

Вы получаете дополнительные 3 ОЗ за каждую имеющуюся у вас классовую способность архетипа рейнджера.
По мере продолжения получения классовых способностей архетипа рейнджера, вы продолжаете получать дополнительные ОЗ таким способом.


.. _arch-feat--Ranger--Advanced-Hunters-Trick:

Улучшенный охотничий трюк (`Advanced Hunter's Trick <https://2e.aonprd.com/Feats.aspx?ID=725>`_) / 6
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- архетип

**Предварительные условия**: :ref:`arch-feat--Ranger--Basic Hunters-Trick`

----------

Вы получаете одну способность рейнджера.
С целью соответствия ее предварительным условиям, ваш уровень рейнджера равен половине уровня персонажа.

**Особенность**: Вы можете выбрать эту способность более одного раза.
Каждый раз, выбирая ее, вы получаете другую способность рейнджера.


.. _arch-feat--Ranger--Master-Spotter:

Мастер наблюдения (`Master Spotter (Ranger) <https://2e.aonprd.com/Feats.aspx?ID=726>`_) / 12
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- архетип

**Предварительные условия**: эксперт Восприятия, :ref:`arch-feat--Ranger--Dedication`

----------

Ваш уровень мастерства Восприятия увеличивается до мастера.





.. include:: /helpers/actions.rst