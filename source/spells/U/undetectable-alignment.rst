.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--u--Undetectable-Alignment:

Необнаружимое мировоззрение (`Undetectable Alignment <http://2e.aonprd.com/Spells.aspx?ID=348>`_) / Закл. 2
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- :uncommon:`необычное`
- преграждение

**Обычай**: сакральный, оккультный

**Использование**: 1 минута (жестовый, словесный, материальный)

**Дистанция**: касание

**Цели**: 1 существо или объект

**Продолжительность**: до следующих ежедневных приготовлений

----------

Вы окутываете существо защитами, которые скрывают его мировоззрение.
Цель выглядит нейтрально, для всех эффектов, которые будут определять ее мировоззрение.





.. include:: /helpers/actions.rst