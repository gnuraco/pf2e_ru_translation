.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--a--Augury:

Предзнаменование (`Augury <https://2e.aonprd.com/Spells.aspx?ID=15>`_) / Закл. 2
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- прорицание
- предсказание

**Обычай**: сакральный, оккультный

**Использование**: 10 минут (материальный, жестовый, словесный)

----------

Вы получаете мимолетный образ будущего.
Во время произнесения этого заклинания спросите о результатах определенного хода действий.
Заклинание может предсказать результат будущего на ближайшие 30 минут и сообщает наилучшее предположение Мастера из следующим возможных результатов:

* **Благо**: Результат будет благоприятным
* **Беда**: Результат будет плохим
* **Благо и беда**: Результаты будут представлять собой смесь хорошего и плохого
* **Ничего**: Не будет результатов которые можно назвать плохими или хорошими

Мастер делает тайную чистую проверку с КС 6.
При провале, результат всегда "Ничего".
Это делает невозможным определить, является ли точным результат "ничего".
Если кто-то спрашивает о том же самом, что и при первом предыдущем колдовстве *предзнаменования*, Мастер использует результат тайной проверки от первого раза.
Однако, если обстоятельства поменялись, возможно что и результат будет другим.




.. include:: /helpers/actions.rst