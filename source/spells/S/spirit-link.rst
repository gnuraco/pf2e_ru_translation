.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--s--Spirit-Link:

Духовная связь (`Spirit Link <http://2e.aonprd.com/Spells.aspx?ID=302>`_) / Закл. 1
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- некромантия
- исцеление

**Обычай**: сакральный, оккультный

**Использование**: |д-2| жестовый, словесный

**Дистанция**: 30 футов

**Цели**: 1 готовая цель

**Продолжительность**: 10 минут

----------

Вы образуете духовную связь с другим существом, которая позволяет вам брать на себя его боль.
Когда вы :ref:`action--Cast-a-Spell` и в начале ваших ходов, если цель имеет меньше максимальных Очков Здоровья, она получает 2 ОЗ (или разницу между его текущими и максимальными ОЗ, если это значение меньше).
Вы теряете столько же ОЗ как и получает цель.

Это духовная передача, так что нет эффектов, которые бы применялись для увеличения получаемых целью или теряемых вами ОЗ.
Передача так же игнорирует любые временные ОЗ которые есть у вас или цели.
Так как эффекты позитивной или негативной энергии не задействованы, *духовная связь* работает даже если вы или цель нежить.
Во время установленной связи, вы не получаете преимуществ от регенерации или быстрого лечения.
Вы можете Развеять это заклинание, а если ваши ОЗ уменьшатся до 0, *духовная связь* закончится автоматически.

----------

**Усиление (+1)**: Количество передаваемых ОЗ увеличивается на 2.





.. include:: /helpers/actions.rst