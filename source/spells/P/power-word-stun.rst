.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--p--Power-Word-Stun:

Слово силы: ошеломление (`Power Word Stun <https://2e.aonprd.com/Spells.aspx?ID=228>`_) / Закл. 8
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- :uncommon:`необычное`
- очарование
- слуховое
- ментальное

**Обычай**: арканный

**Использование**: |д-1| словесный

**Дистанция**: 30 футов

**Цели**: 1 существо

**Продолжительность**: различается

----------

Вы оглушаете цель арканным словом силы.
После использования заклинания на цель, она становится временно иммунной на 10 минут.
Эффект заклинания зависит от уровня цели.

| **13-й или ниже**: Цель "ошеломлена" на 1d6 раундов.
| **14-й, 15-й**: Цель "ошеломлена" на 1 раунд.
| **16-й или больше**: Цель "ослеплена 1".

----------

**Усиление (+1)**: Уровни, для которых применяется каждый результат, увеличиваются на 2.





.. include:: /helpers/actions.rst