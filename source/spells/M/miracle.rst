.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--m--Miracle:

Чудо (`Miracle <https://2e.aonprd.com/Spells.aspx?ID=196>`_) / Закл. 10
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- прорицание

**Обычай**: сакральный

**Использование**: |д-3| жестовый, словесный, материальный

----------

Вы просите помощи непосредственно у вашего божественного источника.
Ваш божественный источник всегда отвергает просьбу, не соответствующую его сущности, и он может удовлетворить другую просьбу (потенциально более мощную или лучше соответствующую его сущности), чем та, о которой вы просили.
Колдовство *чуда* может сделать следующее.

* Повторите любое сакральное заклинание 9-го уровня или ниже
* Повторите любое несакральное заклинание 7-го уровня или ниже
* Произведите любой эффект, уровень силы которого соответствует вышеуказанным эффектам
* Обратите некоторые эффекты, которые относятся к заклинанию *желание*

Мастер может разрешить вам попробовать использовать *чуда* чтобы произвести эффект больший, чем эти, но это может быть опасно, или заклинание может иметь только частичный эффект.





.. include:: /helpers/actions.rst